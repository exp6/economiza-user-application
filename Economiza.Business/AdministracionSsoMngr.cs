﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Economiza.Datamodel;
using Economiza.Entities;
using Economiza.IDatamodel;

namespace Economiza.Business
{
    public class AdministracionSsoMngr
    {
        //--------------------------------------------------------------------------------------
        #region Patrón de diseño Singleton

        private static AdministracionSsoMngr Instance = null;

        /// <summary>
        /// El constructor privado no permite que se genere un constructor por defecto
        /// (que es público)
        /// </summary>
        private AdministracionSsoMngr()
        {
        }

        // creador sincronizado para protegerse de posibles problemas multi-hilo
        // otra prueba para evitar instantiación múltiple 
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void CreateInstance()
        {
            if (Instance == null)
            {
                Instance = new AdministracionSsoMngr();
            }
        }

        public static AdministracionSsoMngr GetInstance()
        {
            if (Instance == null) CreateInstance();
            return Instance;
        }

        #endregion
        //--------------------------------------------------------------------------------------

        #region Métodos Usuario
        /// <summary>
        /// Permite obtener al usuario
        /// procedimiento almacenado: AspNetUsers
        /// </summary>
        /// <param name="usuario">id del usuario</param>
        public UsuarioTO ObtenerUsuarioByEmail(string email)
        {
            try
            {
                IAdministracionSSO daoAdminSSO = DaoFactory.GetFactory().GetDAOAdministracion();
                UsuarioTO resultado = daoAdminSSO.ObtenerUsuarioByEmail(email);
                return resultado;
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }

        /// <summary>
        /// Permite obtener AspNetUserLogins
        /// </summary>
        /// <param name="usuario">email del usuario</param>
        public IQueryable<UsuarioLoginsTO> ObtenerUsuarioLogin(string id)
        {
            try
            {
                IAdministracionSSO daoAdminSSO = DaoFactory.GetFactory().GetDAOAdministracion();
                return daoAdminSSO.ObtenerUsuarioLogin(id);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }

        public void ActualizarFotoPerfil(string email, string urlFoto)
        {
            try
            {
                IAdministracionSSO daoAdminSSO = DaoFactory.GetFactory().GetDAOAdministracion();
                daoAdminSSO.ActualizarFotoPerfil(email, urlFoto);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }

        public bool ActualizarPerfil(UsuarioTO model)
        {
            try
            {
                IAdministracionSSO daoAdminSSO = DaoFactory.GetFactory().GetDAOAdministracion();
                return daoAdminSSO.ActualizarPerfil(model);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }
        #endregion

        #region Métodos AspNetUser
        /// <summary>
        /// Permite obtener un usuario
        /// </summary>
        /// <param name="loginProvider"></param>
        /// <param name="providerKey"></param>
        /// <returns>UsuarioTO</returns>
        public UsuarioTO obtenerUsuarioBySocial(string loginProvider, string providerKey)
        {
            try
            {
                IAdministracionSSO daoAdminSSO = DaoFactory.GetFactory().GetDAOAdministracion();
                UsuarioTO resultado = daoAdminSSO.obtenerUsuarioBySocial(loginProvider, providerKey);
                return resultado;
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }

        /// <summary>
        /// Obtiene un usuario por su nombre de usuario
        /// procedimiento almacenado: AspNetUsers
        /// </summary>
        /// <param name="username"></param>
        /// <returns>UsuarioTO</returns>
        public UsuarioTO obtenerUsuarioByLocal(string username)
        {
            try
            {
                IAdministracionSSO daoAdminSSO = DaoFactory.GetFactory().GetDAOAdministracion();
                return daoAdminSSO.obtenerUsuarioByLocal(username);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }
        #endregion
    }
}
