﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Economiza.Datamodel;
using Economiza.Entities;
using Economiza.IDatamodel;

namespace Economiza.Business
{
    public class IOTMngr
    {
        //--------------------------------------------------------------------------------------
        #region Patrón de diseño Singleton

        private static IOTMngr Instance = null;

        /// <summary>
        /// El constructor privado no permite que se genere un constructor por defecto
        /// (que es público)
        /// </summary>
        private IOTMngr()
        {
        }

        // creador sincronizado para protegerse de posibles problemas multi-hilo
        // otra prueba para evitar instantiación múltiple 
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void CreateInstance()
        {
            if (Instance == null)
            {
                Instance = new IOTMngr();
            }
        }

        public static IOTMngr GetInstance()
        {
            if (Instance == null) CreateInstance();
            return Instance;
        }

        #endregion
        //--------------------------------------------------------------------------------------

        #region Obtener
        public IQueryable<HistoriaUsoTO> ObtenerHistoria30Dias(string channel)
        {
            try
            {
                IIOT daoConfig = DaoFactory.GetFactory().GetDAOIOT();
                return daoConfig.ObtenerHistoria30Dias(channel);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }

        public IQueryable<LogrosTO> ObtenerLogrosUsuario(string channel)
        {
            try
            {
                IIOT daoConfig = DaoFactory.GetFactory().GetDAOIOT();
                return daoConfig.ObtenerLogrosUsuario(channel);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }

        public int ObtenerDiasUsoFiltro(string channel)
        {
            try
            {
                IIOT daoConfig = DaoFactory.GetFactory().GetDAOIOT();
                return daoConfig.ObtenerDiasUsoFiltro(channel);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }
        #endregion
    }
}
