﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Economiza.Datamodel;
using Economiza.Entities;
using Economiza.IDatamodel;

namespace Economiza.Business
{
    public class ListadoMngr
    {
        //--------------------------------------------------------------------------------------
        #region Patrón de diseño Singleton

        private static ListadoMngr Instance = null;

        /// <summary>
        /// El constructor privado no permite que se genere un constructor por defecto
        /// (que es público)
        /// </summary>
        private ListadoMngr()
        {
        }

        // creador sincronizado para protegerse de posibles problemas multi-hilo
        // otra prueba para evitar instantiación múltiple 
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void CreateInstance()
        {
            if (Instance == null)
            {
                Instance = new ListadoMngr();
            }
        }

        public static ListadoMngr GetInstance()
        {
            if (Instance == null) CreateInstance();
            return Instance;
        }

        #endregion
        //--------------------------------------------------------------------------------------

        #region Obtener
        public IQueryable<EstufasTO> ObtenerMarcasEstufas()
        {
            try
            {
                IListado daoConfig = DaoFactory.GetFactory().GetDAOListado();
                return daoConfig.ObtenerMarcasEstufas();
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }

        public IQueryable<ModelosEstufasTO> ObtenerModelosEstufas(int idEstufa)
        {
            try
            {
                IListado daoConfig = DaoFactory.GetFactory().GetDAOListado();
                return daoConfig.ObtenerModelosEstufas(idEstufa);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }

        public IQueryable<TipsTO> ObtenerTips()
        {
            try
            {
                IListado daoConfig = DaoFactory.GetFactory().GetDAOListado();
                return daoConfig.ObtenerTips();
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }
        #endregion
    }
}
