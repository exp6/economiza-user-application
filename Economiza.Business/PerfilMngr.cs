﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Economiza.Datamodel;
using Economiza.Entities;
using Economiza.IDatamodel;

namespace Economiza.Business
{
    public class PerfilMngr
    {
        //--------------------------------------------------------------------------------------
        #region Patrón de diseño Singleton

        private static PerfilMngr Instance = null;

        /// <summary>
        /// El constructor privado no permite que se genere un constructor por defecto
        /// (que es público)
        /// </summary>
        private PerfilMngr()
        {
        }

        // creador sincronizado para protegerse de posibles problemas multi-hilo
        // otra prueba para evitar instantiación múltiple 
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void CreateInstance()
        {
            if (Instance == null)
            {
                Instance = new PerfilMngr();
            }
        }

        public static PerfilMngr GetInstance()
        {
            if (Instance == null) CreateInstance();
            return Instance;
        }

        #endregion
        //--------------------------------------------------------------------------------------

        #region Guardar
        public bool ActualizarPerfil(UserInfoFullViewTO model)
        {
            try
            {
                IPerfil daoConfig = DaoFactory.GetFactory().GetDAOPerfil();
                return daoConfig.ActualizarPerfil(model);
            }
            catch (Exception e)
            {
                Utilities.UtilesLog.LogException(e, this.GetType().ToString());
                throw (e);
            }
        }
        #endregion
    }
}
