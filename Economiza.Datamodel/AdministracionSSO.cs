﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.IDatamodel;
using Economiza.Entities;
using Economiza.Datamodel.Models;
using Economiza.Utilities;
using System.Data.Entity;

namespace Economiza.Datamodel
{
    public class AdministracionSSO : IAdministracionSSO, IDisposable
    {
        private EconomizaEntities db = new EconomizaEntities();

        public AdministracionSSO()
        {
        }

        #region Métodos Usuario

        /// <summary>
        /// Permite obtener al usuario
        /// </summary>
        /// <param name="usuario">id del usuario</param>
        public UsuarioTO ObtenerUsuarioByEmail(string id)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                AspNetUser usuario = db.AspNetUsers.SingleOrDefault(i => i.UserName == id);

                if (usuario == null)
                {
                    return null; // NotFound();
                }

                var user = new UsuarioTO();
                user.Id = usuario.Id;
                user.Email = usuario.Email;
                user.EmailConfirmed = usuario.EmailConfirmed;
                user.UserName = usuario.UserName;

                if (db.usuarios.Any(c => c.IdUsuario == usuario.Id))
                {
                    var cli = db.usuarios.Where(c => c.IdUsuario == usuario.Id).SingleOrDefault();
                    user.DisplayName = cli.DisplayName;
                    user.Picture = string.Empty;
                    user.direccion = cli.direccion;
                    user.telefono = cli.telefono;
                    user.nroKit = (cli.num_kit.HasValue) ? cli.num_kit.Value : 0;
                    if (db.estufa_modelo.Any(c => c.id_estufa_modelo == cli.id_estufa_modelo))
                    {
                        var estufaM = db.estufa_modelo.Where(c => c.id_estufa_modelo == cli.id_estufa_modelo).FirstOrDefault();
                        user.idEstufa = (estufaM.id_estufa.HasValue) ? estufaM.id_estufa.Value : 0;
                    }
                    else
                        user.idEstufa = 0;
                    user.idModeloEstufa = (cli.id_estufa_modelo.HasValue) ? cli.id_estufa_modelo.Value : 0; //cli.idDocumentoIdentidadChile;
                    user.nroCohabitantes = (cli.num_personas_hogar.HasValue) ? cli.num_personas_hogar.Value : 0; //cli.num_personas_hogar;
                    user.cantPromM3 = (cli.prom_compra_lenia.HasValue) ? cli.prom_compra_lenia.Value : 0; //cli.idDocumentoIdentidadOrigen;
                    user.cantPromMeses = (cli.periodo_compra.HasValue) ? cli.periodo_compra.Value : 0; //cli.prom_compra_lenia;
                }
                else
                {
                    user.DisplayName = string.Empty;
                    user.Picture = string.Empty;
                    user.direccion = string.Empty;
                    user.telefono = string.Empty;
                    user.nroKit = 0;
                    user.idModeloEstufa = 0;
                    user.nroCohabitantes = 0;
                    user.cantPromM3 = 0;
                    user.cantPromMeses = 0;
                }

                return user;
            }
            catch (Exception ex)
            {
                Utilities.UtilesLog.LogException(ex, this.GetType().ToString());
                return null;

            }
        }

        /// <summary>
        /// Permite obtener AspNetUserLogins
        /// </summary>
        /// <param name="usuario">email del usuario</param>
        public IQueryable<UsuarioLoginsTO> ObtenerUsuarioLogin(string id)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var list = new List<UsuarioLoginsTO>();
                var _id = db.AspNetUsers.Where(i => i.UserName == id).SingleOrDefault().Id;
                foreach (var item in db.AspNetUserLogins.Where(u => u.UserId == _id))
                {
                    var dato = new UsuarioLoginsTO();
                    dato.LoginProvider = item.LoginProvider;
                    dato.ProviderKey = item.ProviderKey;
                    dato.UserId = item.UserId;
                    list.Add(dato);
                }
                return list.AsQueryable();
            }
            catch (Exception ex)
            {
                Utilities.UtilesLog.LogException(ex, this.GetType().ToString());
                return null;

            }
        }

        /// <summary>
        /// Actualizar Foto Perfil
        /// </summary>
        /// <param name="email">email del usuario</param>
        /// <param name="urlFoto">url foto</param>
        public void ActualizarFotoPerfil(string email, string urlFoto)
        {
            //var userId = db.AspNetUsers.Where(u => u.Email == email).FirstOrDefault().Id;
            //var user = db.usuarios.Where(u => u.IdUsuario == userId).FirstOrDefault();
            //user.Picture = urlFoto;
            //db.Entry(user).State = EntityState.Modified;
            //db.SaveChanges();
        }

        /// <summary>
        /// Actualizar Usuario
        /// </summary>
        public bool ActualizarPerfil(UsuarioTO model)
        {
            try
            {
                var _user = db.AspNetUsers.Where(u => u.UserName == model.UserName).FirstOrDefault();
                if (db.usuarios.Any(d => d.IdUsuario == _user.Id))
                {
                    Utilities.UtilesLog.LogInfo("Modificar usuario", "Economiza.Datamodel.AdministracionSSO.ActualizarPerfil");
                    var item = db.usuarios.Where(d => d.IdUsuario == model.Id).FirstOrDefault();
                    item.DisplayName = model.DisplayName;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    Utilities.UtilesLog.LogInfo("Agregar usuario", "Economiza.Datamodel.AdministracionSSO.ActualizarPerfil");
                    var item = new usuario();
                    item.IdUsuario = _user.Id;
                    item.DisplayName = model.DisplayName;
                    db.usuarios.Add(item);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                Utilities.UtilesLog.LogException(ex, "Economiza.Datamodel.AdministracionSSO.ActualizarPerfil");
                return false;
            }
        }
        #endregion

        #region Métodos AspNetUsers

        #region obtenerUsuarioBySocial
        /// <summary>
        /// Permite obtener un usuario
        /// sp: AspNetUserLogins
        /// </summary>
        /// <param name="loginProvider"></param>
        /// <param name="providerKey"></param>
        /// <returns>UsuarioTO</returns>
        public UsuarioTO obtenerUsuarioBySocial(string loginProvider, string providerKey)
        {
            //var UserId = db.AspNetUserLogins.Where(u => u.LoginProvider == loginProvider && u.ProviderKey == providerKey).FirstOrDefault().UserId;
            //var _user = db.AspNetUsers.Find(UserId);
            //var user = new UsuarioTO();

            //user.Id = _user.Id;
            //user.UserName = _user.UserName;
            //user.Email = _user.Email;
            //user.SsoPerfils = new List<PerfilTO>();
            //foreach (var p in _user.SsoPerfils)
            //{
            //    var dato = new PerfilTO();
            //    dato.idPerfil = p.idPerfil;
            //    dato.nombrePerfil = p.nombrePerfil;
            //    user.SsoPerfils.Add(dato);
            //}

            //return user;
            return null;
        }
        #endregion

        #region obtenerUsuarioByLocal
        /// <summary>
        /// Obtiene un usuario por su nombre de usuario
        /// procedimiento almacenado: AspNetUsers
        /// </summary>
        /// <param name="username"></param>
        /// <returns>UsuarioTO</returns>
        public UsuarioTO obtenerUsuarioByLocal(string username)
        {
            var UserId = db.AspNetUsers.Where(u => u.UserName == username).FirstOrDefault().Id;
            var _user = db.AspNetUsers.Find(UserId);
            var user = new UsuarioTO();

            user.Id = _user.Id;
            user.UserName = _user.UserName;
            user.Email = _user.Email;
            Utilities.UtilesLog.LogInfo("Usuario conectado es: " + user.UserName, this.GetType().ToString());
            return user;
        }
        #endregion

        #endregion


        public void Dispose()
        {
            try
            {
                db.Dispose();
            }
            catch { }
        }
    }
}
