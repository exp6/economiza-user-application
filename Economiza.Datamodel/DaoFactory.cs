﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.IDatamodel;

namespace Economiza.Datamodel
{
    public class DaoFactory : IDaoFactory
    {
        /// <summary>
        /// Realiza la conexión a la Base de Datos con el proveedor de conexión.
        /// </summary>
        public DaoFactory()
        {
        }

        #region Instancias DAO

        #region Administración Autorización

        /// <summary>
        /// Obtiene instancia del DAO de Administracion
        /// </summary>
        public override IAdministracionSSO GetDAOAdministracion()
        {
            return new AdministracionSSO();
        }

        #endregion

        #region Perfil

        /// <summary>
        /// Obtiene instancia del DAO de Perfil
        /// </summary>
        public override IPerfil GetDAOPerfil()
        {
            return new Perfil();
        }

        #endregion

        #region Listado

        /// <summary>
        /// Obtiene instancia del DAO de Listado
        /// </summary>
        public override IListado GetDAOListado()
        {
            return new Listado();
        }

        #endregion

        #region IOT

        /// <summary>
        /// Obtiene instancia del DAO de IOT
        /// </summary>
        public override IIOT GetDAOIOT()
        {
            return new IOT();
        }

        #endregion

        #endregion
    }
}
