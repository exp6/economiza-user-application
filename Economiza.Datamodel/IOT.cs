﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.IDatamodel;
using Economiza.Entities;
using Economiza.Datamodel.Models;
using Economiza.Utilities;
namespace Economiza.Datamodel
{
    public class IOT : IIOT, IDisposable
    {
        private economizaIOTEntities db = new economizaIOTEntities();

        #region Obtener
        /// <summary>
        /// Permite obtener los datos de los ultimos 30 dias del filtro
        /// </summary>
        /// <returns></returns>
        public IQueryable<HistoriaUsoTO> ObtenerHistoria30Dias(string channel)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var list = new List<HistoriaUsoTO>();
            foreach (var item in db.GET_HISTORIA_O2_TEMP_30_DIAS(channel))
            {
                var dato = new HistoriaUsoTO();
                dato.fecha = formatFecha(item.FECHA);
                dato.fechaFormat = formatFecha(item.FECHA).ToString("dd-MM-yyyy");
                dato.PROM_O2 = item.PROM_O2.HasValue ? item.PROM_O2.Value : 0;
                dato.PROM_TEMP = item.PROM_TEMP.HasValue ? item.PROM_TEMP.Value : 0;
                dato.PROM_CO = item.PROM_CO.HasValue ? item.PROM_CO.Value : 0;
                dato.PROM_MP = item.PROM_MP.HasValue ? item.PROM_MP.Value : 0;
                list.Add(dato);
            }
            return list.AsQueryable();
        }

        /// <summary>
        /// Permite obtener los logros del usuario
        /// </summary>
        /// <returns></returns>
        public IQueryable<LogrosTO> ObtenerLogrosUsuario(string channel)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var list = new List<LogrosTO>();
            foreach (var item in db.GET_LOGROS_USUARIO(channel))
            {
                var dato = new LogrosTO();
                dato.ID = item.ID;
                dato.TITULO = item.TITULO;
                dato.LOGRO = item.LOGRO;
                list.Add(dato);
            }
            return list.AsQueryable();
        }

        /// <summary>
        /// Permite obtener la cantidad de dias de uso del filtro
        /// </summary>
        /// <returns></returns>
        public int ObtenerDiasUsoFiltro(string channel)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var dias = db.GET_DIAS_USO_FILTRO(channel).SingleOrDefault();
            if (dias.HasValue)
                return dias.Value;
            else
                return 0;
        }
        #endregion

        #region helper
        private DateTime formatFecha(string fechaIn)
        {
            var año = Convert.ToInt32(fechaIn.Substring(0, 4));
            var mes = Convert.ToInt32(fechaIn.Substring(4, 2));
            var dia = Convert.ToInt32(fechaIn.Substring(6, 2));
            return new DateTime(año, mes, dia);
        }
        #endregion

        public void Dispose()
        {
            try
            {
                db.Dispose();
            }
            catch { }
        }
    }
}
