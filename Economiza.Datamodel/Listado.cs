﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.IDatamodel;
using Economiza.Entities;
using Economiza.Datamodel.Models;
using Economiza.Utilities;
namespace Economiza.Datamodel
{
    public class Listado : IListado, IDisposable
    {
        private EconomizaEntities db = new EconomizaEntities();

        #region Obtener
        /// <summary>
        /// Permite obtener el listado de estufas
        /// </summary>
        /// <returns></returns>
        public IQueryable<EstufasTO> ObtenerMarcasEstufas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var list = new List<EstufasTO>();
            foreach (var item in db.estufas)
            {
                var dato = new EstufasTO();
                dato.idEstufa = item.id_estufa;
                dato.codigo = item.codigo.HasValue ? item.codigo.Value : 0;
                dato.marca = item.marca;
                list.Add(dato);
            }
            return list.AsQueryable();
        }

        /// <summary>
        /// Permite obtener el listado de modelos
        /// </summary>
        /// <returns></returns>
        public IQueryable<ModelosEstufasTO> ObtenerModelosEstufas(int idEstufa)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var list = new List<ModelosEstufasTO>();
            foreach (var item in db.estufa_modelo.Where(m => m.id_estufa == idEstufa))
            {
                var dato = new ModelosEstufasTO();
                dato.idModeloEstufa = item.id_estufa_modelo;
                dato.idEstufa = item.id_estufa.HasValue ? item.id_estufa.Value : 0;
                dato.codigo = item.codigo.HasValue ? item.codigo.Value : 0;
                dato.modelo = item.modelo;
                list.Add(dato);
            }
            return list.AsQueryable();
        }

        /// <summary>
        /// Permite obtener el listado de estufas
        /// </summary>
        /// <returns></returns>
        public IQueryable<TipsTO> ObtenerTips()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var list = new List<TipsTO>();
            foreach (var item in db.tips)
            {
                var dato = new TipsTO();
                dato.Id = item.id_tip;
                dato.Codigo = item.codigo.HasValue ? item.codigo.Value : 0;
                dato.Titulo = item.titulo;
                dato.Descripcion = item.texto;
                list.Add(dato);
            }
            return list.AsQueryable();
        }
        #endregion

        public void Dispose()
        {
            try
            {
                db.Dispose();
            }
            catch { }
        }
    }
}
