﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.Entities;
using Economiza.IDatamodel;
using Economiza.Datamodel.Models;
using System.Data.Entity;

namespace Economiza.Datamodel
{
    public class Perfil : IPerfil
    {
        private EconomizaEntities db = new EconomizaEntities();

        /// <summary>
        /// Guardar datos basicos del cliente
        /// </summary>
        /// <param name="model">UserInfoFullViewTO</param>
        public bool ActualizarPerfil(UserInfoFullViewTO model)
        {
            try
            {
                Utilities.UtilesLog.LogInfo("ActualizarPerfil()", "Economiza.Datamodel.Perfil.ActualizarPerfil");
                string idUser = string.Empty;

                var user = db.AspNetUsers.Where(u => u.Email == model.Email).FirstOrDefault();
                idUser = user.Id;

                if (db.usuarios.Any(d => d.IdUsuario == idUser))
                {
                    Utilities.UtilesLog.LogInfo("Modificar Cliente", "Economiza.Datamodel.Perfil.ActualizarPerfil");
                    //Modificar
                    var item = db.usuarios.Where(d => d.IdUsuario == idUser).FirstOrDefault();
                    item.DisplayName = model.DisplayName;
                    item.telefono = model.telefono;
                    item.direccion = model.direccion;
                    item.num_personas_hogar = model.nroCohabitantes;
                    item.prom_compra_lenia = model.cantPromM3;
                    item.periodo_compra = model.cantPromMeses;
                    item.num_kit = model.nroKit;
                    item.id_estufa_modelo = model.idModeloEstufa;
                    item.fechaModificacion = DateTime.Now;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    Utilities.UtilesLog.LogInfo("Agregar Cliente", "Economiza.Datamodel.Perfil.ActualizarPerfil");
                    //Agregar
                    var item = new usuario();
                    item.IdUsuario = idUser;
                    item.DisplayName = model.DisplayName;
                    item.telefono = model.telefono;
                    item.direccion = model.direccion;
                    item.num_personas_hogar = model.nroCohabitantes;
                    item.prom_compra_lenia = model.cantPromM3;
                    item.periodo_compra = model.cantPromMeses;
                    item.num_kit = model.nroKit;
                    item.id_estufa_modelo = model.idModeloEstufa;
                    item.fechaModificacion = DateTime.Now;
                    db.usuarios.Add(item);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                Utilities.UtilesLog.LogException(ex, "Economiza.Datamodel.Perfil.ActualizarPerfil");
                return false;
            }
        }
    }
}
