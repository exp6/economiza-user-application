﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economiza.Entities
{
    public class EmailTO
    {
        public string emailDestino { get; set; }
        public string asunto { get; set; }
        public string mensaje { get; set; }
    }
}