﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economiza.Entities
{
    public class HistoriaUsoTO
    {
        public DateTime fecha { get; set; }
        public string fechaFormat { get; set; }
        public double PROM_O2 { get; set; }
        public double PROM_TEMP { get; set; }
        public double PROM_CO { get; set; }
        public double PROM_MP { get; set; }
    }

    public class LogrosTO
    {
        public int ID { get; set; }
        public string TITULO { get; set; }
        public string LOGRO { get; set; }
    }
}