﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economiza.Entities
{
    public class EstufasTO
    {
        public int idEstufa { get; set; }
        public int codigo { get; set; }
        public string marca { get; set; }
    }

    public class ModelosEstufasTO
    {
        public int idModeloEstufa { get; set; }
        public int idEstufa { get; set; }
        public int codigo { get; set; }
        public string modelo { get; set; }
    }



    public class TipsTO
    {
        public int Id { get; set; }
        public int Codigo { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
    }
}