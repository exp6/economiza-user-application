﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economiza.Entities
{
    public enum TipoPerfil
    {
        SinAutenticacion = 0,
        Administrador = 1,
        Supervisor = 2,
        Pagador = 3
    }
    public class TipoPerfilRoles
    {
        public const string Administrador = "Administrador";
        public const string Supervisor = "Supervisor";
        public const string JefeTurno = "JefeTurno";
    }
    public class PerfilTO
    {
        public PerfilTO() { }

        public PerfilTO(int idPerfil, string nombrePerfil)
        {
            this.idPerfil = idPerfil;
            this.nombrePerfil = nombrePerfil;
        }

        public int idPerfil { get; set; }
        public string nombrePerfil { get; set; }
    }
}
