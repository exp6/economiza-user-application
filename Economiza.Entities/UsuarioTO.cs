﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economiza.Entities
{
    public class UsuarioTO: ClienteTO
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string UserName { get; set; }
    }

    public class ClienteTO
    {
        public string DisplayName { get; set; }
        public string Picture { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public long nroKit { get; set; }
        public int idEstufa { get; set; }
        public int idModeloEstufa { get; set; }
        public int nroCohabitantes { get; set; }
        public int cantPromM3 { get; set; }
        public int cantPromMeses { get; set; }
    }

    public class UsuarioLoginsTO
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string UserId { get; set; }
    }

    #region Facebook
    public class FacebookDataTO
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public FacebookPictureContTO picture { get; set; }
    }

    public class FacebookPictureContTO
    {
        public FacebookPictureTO data { get; set; }
    }

    public class FacebookPictureTO
    {
        public bool is_silhouette { get; set; }
        public string url { get; set; }
    }
    #endregion

    public class UserInfoFullViewTO
    {
        public string Email { get; set; }
        public bool HasRegistered { get; set; }
        public string LoginProvider { get; set; }
        public string DisplayName { get; set; }
        public string Picture { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public long nroKit { get; set; }
        public int idEstufa { get; set; }
        public int idModeloEstufa { get; set; }
        public int nroCohabitantes { get; set; }
        public int cantPromM3 { get; set; }
        public int cantPromMeses { get; set; }
    }
}
