﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.Entities;

namespace Economiza.IDatamodel
{
    public interface IAdministracionSSO
    {
        /// <summary>
        /// Permite obtener al usuario
        /// </summary>
        /// <param name="usuario">id del usuario</param>
        UsuarioTO ObtenerUsuarioByEmail(string id);
        IQueryable<UsuarioLoginsTO> ObtenerUsuarioLogin(string id);
        void ActualizarFotoPerfil(string email, string urlFoto);

        bool ActualizarPerfil(UsuarioTO model);

        UsuarioTO obtenerUsuarioBySocial(string loginProvider, string providerKey);
        UsuarioTO obtenerUsuarioByLocal(string username);
    }
}
