﻿using System;
using System.Collections;

namespace Economiza.IDatamodel
{
    public abstract class IDaoFactory
    {
        #region Factory
        private static IDaoFactory _Factory;

        //TODO:Por ahora se llena a través de un método, pero debería llenarse a través de una lectura 
        //desde un archivo de configuracion propio del bussiness

        private static Hashtable _AssemblyList;

        public Hashtable AssemblyList
        {
            get { return _AssemblyList; }
            set { _AssemblyList = value; }
        }

        /// <summary>
        /// Inicializa la instancia con los valores especificados.
        /// </summary>
        protected IDaoFactory()
        {
        }

        /// <summary>
        /// Obtiene una instancia de la fábrica de DAO.
        /// </summary>
        /// <returns></returns>
        public static IDaoFactory GetFactory()
        {
            return _Factory;
        }

        /// <summary>
        /// Establece la implementación de la fábrica de DAO.
        /// </summary>
        /// <param name="factory"></param>
        public static void SetFactory(IDaoFactory factory)
        {
            _Factory = factory;
        }

        #endregion

        #region Instancias DAO

        #region Administración Autorización

        /// <summary>
        /// Obtiene instancia del DAO de Administracion
        /// </summary>
        public abstract IAdministracionSSO GetDAOAdministracion();

        #endregion

        #region Perfil

        /// <summary>
        /// Obtiene instancia del DAO de Perfil
        /// </summary>
        public abstract IPerfil GetDAOPerfil();

        #endregion

        #region Listado

        /// <summary>
        /// Obtiene instancia del DAO de Listado
        /// </summary>
        public abstract IListado GetDAOListado();

        #endregion

        #region IOT

        /// <summary>
        /// Obtiene instancia del DAO de IOT
        /// </summary>
        public abstract IIOT GetDAOIOT();

        #endregion
        #endregion
    }
}
