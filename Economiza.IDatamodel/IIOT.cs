﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.Entities;

namespace Economiza.IDatamodel
{
    public interface IIOT
    {
        #region Obtener
        IQueryable<HistoriaUsoTO> ObtenerHistoria30Dias(string channel);
        IQueryable<LogrosTO> ObtenerLogrosUsuario(string channel);
        int ObtenerDiasUsoFiltro(string channel);
        #endregion
    }
}
