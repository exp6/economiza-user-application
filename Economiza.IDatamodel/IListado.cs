﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.Entities;

namespace Economiza.IDatamodel
{
    public interface IListado
    {
        #region Obtener
        IQueryable<EstufasTO> ObtenerMarcasEstufas();
        IQueryable<ModelosEstufasTO> ObtenerModelosEstufas(int idEstufa);
        IQueryable<TipsTO> ObtenerTips();
        #endregion
    }
}
