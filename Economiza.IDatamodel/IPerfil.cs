﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Economiza.Entities;

namespace Economiza.IDatamodel
{
    public interface IPerfil
    {
        #region Guardar
        bool ActualizarPerfil(UserInfoFullViewTO model);
        #endregion
    }
}
