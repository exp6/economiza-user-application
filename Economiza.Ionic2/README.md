This is a starter template for [Ionic](http://ionicframework.com/docs/) projects.

## How to use this template

*This template does not work on its own*. The shared files for each starter are found in the [ionic2-app-base repo](https://github.com/ionic-team/ionic2-app-base).

To use this template, either create a new ionic project using the ionic node.js utility, or copy the files from this repository into the [Starter App Base](https://github.com/ionic-team/ionic2-app-base).

### With the Ionic CLI:

Take the name after `ionic2-starter-`, and that is the name of the template to be used when using the `ionic start` command below:

```bash
$ sudo npm install -g ionic cordova
$ ionic start mySideMenu sidemenu
```

Then, to run it, cd into `mySideMenu` and run:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```
ionic cordova build android --prod --release
ionic cordova run android --device
ionic cordova run android --device  --prod --release

ionic cordova build android --prod --release --buildConfig=build.json
ionic cordova run android --prod --release --buildConfig=build.json

sudo npm install -g ios-deploy --unsafe-perm=true

Substitute ios for android if not on a Mac.


###################################
###        Notas Basedos       ####
###################################

### En caso de problemas al ejecutar: $ npm install 
Paso 1: $ ionic state restore
Paso 2: $ npm install

### No ejecutar: $ npm cache clean
Debe ejecutar: $ npm cache verify

### En caso de problemas al ejecutar: $ ionic serve:  
### Error: Cannot find module '@angular/tsc-wrapped/src/tsc'
Paso 1: $ npm install @angular/tsc-wrapped autoprefixer

### Instalar una versión de npm especifica
npm install -g npm@5.2.0

###################################
###            UPLOAD          ####
###################################
Before your app can show up in View, you need to upload it:

$ ionic upload


###################################
###      PASO 1: KEYSTORE      ####
###################################
C:\Program Files\Java\jdk1.8.0_131\bin>keytool -genkey -v -keystore D:\Basedos\ECOnomiza\Fuentes\Economiza.Ionic2\economiza-apps.keystore -alias appseconomiza -keyalg RSA -keysize 2048 -validity 100000

Introduzca la contraseña del almacén de claves:
Volver a escribir la contraseña nueva: basedos
¿Cuáles son su nombre y su apellido?
  [Unknown]:  Enrique Hernandez
¿Cuál es el nombre de su unidad de organización?
  [Unknown]:  basedos
¿Cuál es el nombre de su organización?
  [Unknown]:  basedos
¿Cuál es el nombre de su ciudad o localidad?
  [Unknown]:  santiago
¿Cuál es el nombre de su estado o provincia?
  [Unknown]:  santiago
¿Cuál es el código de país de dos letras de la unidad?
  [Unknown]:  56
¿Es correcto CN=Enrique Hernandez, OU=basedos, O=basedos, L=santiago, ST=santiago, C=56?
  [no]:  s

Generando par de claves RSA de 2.048 bits para certificado autofirmado (SHA256withRSA) con una validez de 100.000 días
        para: CN=Enrique Hernandez, OU=basedos, O=basedos, L=santiago, ST=santiago, C=56
Introduzca la contraseña de clave para <appseconomiza>
        (INTRO si es la misma contraseña que la del almacén de claves):
[Almacenando economiza-apps.keystore]

E:\Basedos\ECOnomiza\Fuentes\Economiza.Ionic2>

###################################
###     PASO 2: BUILD APK      ####
###################################
ionic cordova build android --prod --release

###################################
###     PASO 3: SIGN APK       ####
###################################
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore economiza-apps.keystore E:\Basedos\ECOnomiza\Fuentes\Economiza.Ionic2\platforms\android\build\outputs\apk\android-release-unsigned.apk appseconomiza



#########
export JAVA_HOME=$(/usr/libexec/java_home)

export ANDROID_HOME="/Users/enriquehernandezleiva/Library/Android/sdk"
export PATH="${PATH}:/$ANDROID_HOME/platform-tools:/$ANDROID_HOME/tools:/$ANDROID_HOME/tools/bin"

export PATH=$PATH:/Users/enriquehernandezleiva/opt/gradle/bin