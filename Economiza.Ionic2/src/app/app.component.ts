import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { SERVICIO_API } from '../shared/baseurl.constants';
import { Nav, Platform, ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Observable } from 'rxjs/Observable';

import { PubNubAngular } from 'pubnub-angular2';

// import { ParameterService } from '../modules/services/parameters-behaviors';
import { AuthService } from '../modules/services/account/auth.service';

import { DashboardPage } from '../pages/dashboard/dashboard';
import { CombustionPage } from '../pages/combustion/combustion';
import { TipsPage } from '../pages/tips/tips';
import { ContactoPage } from '../pages/contacto/contacto';
import { AboutPage } from '../pages/about/about';
import { RegistroPage } from '../pages/registro/registro';
import { GlosarioPage } from '../pages/glosario/glosario';
import { Button } from 'ionic-angular/components/button/button';
// import { DatosPage } from '../pages/datos/datos';
// import { ComprasPage } from '../pages/compras/compras';
// import { LogrosPage } from '../pages/logros/logros';
// import { ReportesPage } from '../pages/reportes/reportes';
// import { AlertasPage } from '../pages/alertas/alertas';
// import { MantencionPage } from '../pages/mantencion/mantencion';
// import { TutorialPage } from '../pages/tutorial/tutorial';

import { IPerfil } from '../models/Perfil';
import { UsuarioService } from '../modules/services/usuario.service';
import { BoundCallbackObservable } from 'rxjs/observable/BoundCallbackObservable';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = RegistroPage;
  pages: Array<{ divider: string, icon: string, title: string, component: any }>;
  public userAutenticado: Observable<boolean>;
  public perfil: IPerfil;
  public obtuvoMensajeEon: boolean = false;

  toastInternet = this.toastCtrl.create({
    message: 'Sin internet',
    duration: 300000,
    position: 'top'
  });

  toastFiredStatus = this.toastCtrl.create({
    message: 'Mala Combustión',
    duration: 300000,
    position: 'top'
  });

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private authService: AuthService,
    // private parameterService: ParameterService,
    private network: Network,
    private _http: Http,
    private toastCtrl: ToastController,
    public usuarioService: UsuarioService,
    public pubnubService: PubNubAngular
  ) {
    this.initializeApp();

    this.userAutenticado = this.authService.isAuthenticated();

    // used for an example of ngFor and navigation
    this.pages = [
      { divider: '', icon: 'clipboard', title: 'Home', component: DashboardPage },
      { divider: '', icon: 'flame', title: 'Calidad de Combustión', component: CombustionPage },
      { divider: '', icon: 'sunny', title: 'Recomendaciones y datos', component: TipsPage },
      { divider: 'divider', icon: 'book', title: 'Glosario', component: GlosarioPage },
      // { divider: 'divider', icon: 'cart', title: 'Compras', component: ComprasPage },
      { divider: '', icon: 'mail', title: 'Contacto', component: ContactoPage },
      { divider: '', icon: 'information-circle', title: 'Acerca de...', component: AboutPage },
      // { divider: '', icon: 'person', title: 'Registro', component: RegistroPage },
      // { divider: '', icon: 'person', title: 'Datos', component: DatosPage },
      // { divider: '', icon: 'person', title: 'Logros', component: LogrosPage },
      // { divider: '', icon: 'person', title: 'Tips', component: TipsPage },
      // { divider: '', icon: 'person', title: 'Alertas', component: AlertasPage },
      // { divider: '', icon: 'person', title: 'Mantención', component: MantencionPage },
      // { divider: '', icon: 'person', title: 'Tutorial de Mantención', component: TutorialPage },
      // { divider: '', icon: 'person', title: 'Reportes', component: ReportesPage }
    ];

    this.usuarioService.getPerfil().subscribe(result => {
      // console.log('result', result);
      this.perfil = result as IPerfil;
      this.llamarPubNub();
    });
  }

  llamarPubNub() {
    // console.log('llamarPubNub');
    this.pubnubService.init({
      publishKey: 'pub-c-56a38c54-26de-4c6a-99d2-9cb7b7e3db29',
      subscribeKey: 'sub-c-cda52064-6758-11e7-9bf2-0619f8945a4f'
    });

    // pubnubService.addListener({
    //   status: function (st) {
    //     if (st.category === "PNUnknownCategory") {
    //       var newState = { new: 'error' };
    //       pubnubService.setState({
    //         state: newState
    //       },
    //         function (status) {
    //           console.log(st.errorData.message);
    //         });
    //     }
    //   },
    //   message: function (message) {
    //     if (message && message.message) {
    //       let jsonPubNub = JSON.parse(message.message);
    //       // console.log('pubnubService.addListener', jsonPubNub);
    //       let firestatus = false;
    //       if (jsonPubNub.firestatus == 0)
    //         firestatus = true;
    //       // this.checkFiredStatusMessage(firestatus);
    //     }
    //   }
    // });

    this.pubnubService.subscribe({
      channels: [this.perfil.nroKit],
      triggerEvents: true,
      // withPresence: true,
      // autoload: 100,
      // triggerEvents: ['message']
    });

    // pubnubService.release('265001');
    // pubnubService.clean('265001');
    // var myStack1 = pubnubService.getMessage('265001');
    // console.log('llamarPubNub nroKit', this.perfil.nroKit);
    var myStack1 = this.pubnubService.getMessage(this.perfil.nroKit, () => {
      // console.log('myStack2', myStack1.length, myStack1);
      // pubnubService.release('265001');
      if (myStack1.length > 2)
        this.pubnubService.clean(this.perfil.nroKit);
      if (myStack1 && myStack1[myStack1.length - 1] && myStack1[myStack1.length - 1].message) {
        let jsonPubNub = JSON.parse(myStack1[myStack1.length - 1].message);
        // console.log('pubnubService.getMessage', jsonPubNub);
        this.obtuvoMensajeEon = true;
        let firestatus = false;
        if (jsonPubNub.firestatus == 0)
          firestatus = true;
        this.checkFiredStatusMessage(firestatus);
      }
      // setTimeout(function () {
      //   pubnubService.clean('265001');
      // }, 150);
    });
    // console.log('myStack1', myStack1);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // this.nav.viewDidEnter.subscribe((view) => {
      //   this.checkFiredStatusMessage(false);
      //   this.usuarioService.getPerfil().subscribe(result => {
      //     this.perfil = result as IPerfil;
      //     if (view.instance.constructor.name == 'DatosPage') {
      //       this.pubnubService.init({
      //         publishKey: 'pub-c-56a38c54-26de-4c6a-99d2-9cb7b7e3db29',
      //         subscribeKey: 'sub-c-cda52064-6758-11e7-9bf2-0619f8945a4f'
      //       });
      //       this.pubnubService.unsubscribe({
      //         channels: [this.perfil.nroKit]
      //       })
      //     } else {
      //       this.llamarPubNub();
      //     }
      //   });
      // });

      this._http.get(`${SERVICIO_API}/ApiCheck/Internet`).subscribe(() => {
        // console.log('network connected check!');
        this.checkInternetMessage(true);
      }, (err) => {
        // console.log('network was disconnected :-(', err);
        this.checkInternetMessage(false);
      });

      // watch network for a disconnect
      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        // console.log('network was disconnected :-(');
        this.checkInternetMessage(false);
      });

      // watch network for a connection
      let connectSubscription = this.network.onConnect().subscribe(() => {
        // console.log('network connected!');
        this.checkInternetMessage(true);
      });

      // this.fireStatusBehaviorSubject.subscribe(result => {
      //   console.log('fireStatusBehaviorSubject check!', result);
      //   this.checkFiredStatusMessage(result);
      // }, (err) => {
      //   // console.log('network was disconnected :-(', err);
      //   this.checkFiredStatusMessage(false);
      // });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, { 'fromMenu': true });
  }

  checkInternetMessage(check: boolean) {
    if (!this.toastInternet) {
      this.toastInternet = this.toastCtrl.create({
        message: 'Sin internet',
        duration: 300000,
        position: 'top'
      });
    }
    if (!check) {
      this.toastInternet.present();
    } else
      this.toastInternet.dismiss();
  }

  public checkFiredStatusMessage(check: boolean) {
    if (!this.toastFiredStatus) {
      this.toastFiredStatus = this.toastCtrl.create({
        message: 'Mala Combustión',
        duration: 300000,
        position: 'top'
      });
    }
    if (check) {
      this.toastFiredStatus.present();
    } else
      this.toastFiredStatus.dismiss();
  }

  public logout() {
    this.authService.signOut();
    this.userAutenticado = this.authService.isAuthenticated();
    this.nav.setRoot(RegistroPage);
  }
}
