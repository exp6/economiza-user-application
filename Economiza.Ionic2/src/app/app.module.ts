import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

import { PubNubAngular } from 'pubnub-angular2';

import { MyApp } from './app.component';
import { BtLogoComponent } from '../components/bt-logo/bt-logo';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { TipsPage } from '../pages/tips/tips';
import { RegistroPage } from '../pages/registro/registro';
import { LoginPage } from '../pages/login/login';
// import { LoginComponent } from '../pages/registro/login/login.component';
// import { RegistroComponent } from '../pages/registro/registro/registro.component';
import { DatosPage } from '../pages/datos/datos';
import { ComprasPage } from '../pages/compras/compras';
import { LogrosPage } from '../pages/logros/logros';
import { CombustionPage } from '../pages/combustion/combustion';
import { ReportesPage } from '../pages/reportes/reportes';
import { ContactoPage } from '../pages/contacto/contacto';
import { AboutPage } from '../pages/about/about';
import { AlertasPage } from '../pages/alertas/alertas';
import { MantencionPage } from '../pages/mantencion/mantencion';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { GlosarioPage } from '../pages/glosario/glosario';

import { ParameterService } from '../modules/services/parameters-behaviors';
import { AuthService } from '../modules/services/account/auth.service';
import { ProfileService } from '../modules/services/account/profile.service';
import { TokenService } from '../modules/services/account/token.service';
import { AccountService } from '../modules/services/account/account.service';
import { TransferHttpModule } from '../modules/transfer-http/transfer-http.module';
import { LogrosService } from '../modules/services/logros.service';
import { TipsService } from '../modules/services/tips.service';
import { OrigenService } from '../modules/services/origen.service';
// import { MarcasService } from '../modules/services/estufas.service';
// import { ModelosService } from '../modules/services/estufas.service';
import { ListadosService } from '../modules/services/listados.service';
import { UsuarioService } from '../modules/services/usuario.service';
import { GlosarioService } from '../modules/services/glosario.service';
import { EmailService } from '../modules/services/email.service';
import { IotService } from '../modules/services/iot.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlertaComponent } from '../components/alerta/alerta';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '47aef63c'
  }
};

@NgModule({
  declarations: [
    MyApp,
    DashboardPage,
    TipsPage,
    LoginPage,
    RegistroPage,
    DatosPage,
    ComprasPage,
    LogrosPage,
    CombustionPage,
    ReportesPage,
    ContactoPage,
    AboutPage,
    AlertasPage,
    MantencionPage,
    TutorialPage,
    GlosarioPage,
    BtLogoComponent,
    AlertaComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    TransferHttpModule,
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashboardPage,
    TipsPage,
    LoginPage,
    RegistroPage,
    DatosPage,
    ComprasPage,
    LogrosPage,
    CombustionPage,
    ReportesPage,
    ContactoPage,
    AboutPage,
    AlertasPage,
    MantencionPage,
    TutorialPage,
    GlosarioPage
  ],
  providers: [
    Network,
    PubNubAngular,
    LogrosService,
    TipsService,
    OrigenService,
    // MarcasService,
    // ModelosService,
    ListadosService,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ProfileService,
    TokenService,
    ParameterService,
    AuthService,
    AccountService,
    UsuarioService,
    GlosarioService,
    EmailService,
    IotService
  ]
})
export class AppModule { }
