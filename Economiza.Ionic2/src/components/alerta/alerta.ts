import { Component } from '@angular/core';

@Component({
  selector: 'alerta',
  templateUrl: 'alerta.html'
})
export class AlertaComponent {

  textAlert: string;
  showAlert: boolean = false;

  constructor() {
    this.textAlert = 'Alerta de incendio';
  }

}
