import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AlertasPage } from '../../pages/alertas/alertas';
import { DashboardPage } from '../../pages/dashboard/dashboard';

@Component({
  selector: 'bt-logo',
  templateUrl: 'bt-logo.html'
})
export class BtLogoComponent {

  public cantAlert: number = 4;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  goLink() {
    if (this.cantAlert > 0){
      this.navCtrl.setRoot(AlertasPage);
    } else {
      this.navCtrl.setRoot(DashboardPage);
    }
  }
}