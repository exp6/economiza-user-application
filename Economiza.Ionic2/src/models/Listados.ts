export interface IEstufa {
    idEstufa: number;
    codigo: number;
    marca: string;
}

export interface IModeloEstufa {
    idModeloEstufa: number;
    idEstufa: number;
    codigo: number;
    modelo: string;
}