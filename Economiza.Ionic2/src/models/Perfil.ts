export class IPerfil {
    Id: string;
    Email: string;
    HasRegistered: boolean;
    LoginProvider: string;
    DisplayName: string;
    Picture: string;
    telefono: string;
    nroKit: string;
    direccion: string;
    idEstufa: number;
    idModeloEstufa: number;
    nroCohabitantes: number;
    cantPromM3: number;
    cantPromMeses: number;
}