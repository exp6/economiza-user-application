export interface IEmail {
    emailDestino: string;
    asunto: string;
    mensaje: string;
}