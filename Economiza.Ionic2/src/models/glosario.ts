export interface IGlosario {
    Id: number;
    Palabra: string;
    Definicion: string;
}