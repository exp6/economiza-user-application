import { DateTime } from "ionic-angular/components/datetime/datetime";

export interface IHistoriaUso {
    fecha: DateTime;
    fechaFormat: string;
    PROM_O2: number;
    PROM_TEMP: number;
    PROM_CO: number;
    PROM_MP: number;
}

export interface ILogro {
    ID: number;
    TITULO: string;
    LOGRO: string;
}