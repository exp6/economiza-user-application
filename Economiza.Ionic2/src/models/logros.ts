export interface ILogros {
    Id: number;
    Titulo: string;
    Descripcion: string;
    Categoria: string;
    Logrado: boolean;
}