// import { IMarcas } from '../../models/estufas';
// import { IidModeloEstufas } from '../../models/estufas';
import { IEstufa, IModeloEstufa } from '../../models/Listados';

export const MarcasMock: IEstufa[] = [
    { idEstufa: 1, codigo: 1, marca: 'Amesti' },
    { idEstufa: 2, codigo: 1, marca: 'Bosca' }
];

export const ModelosMock: IModeloEstufa[] = [
    { idModeloEstufa: 1, codigo: 1, modelo: 'Classic 400', idEstufa: 1 },
    { idModeloEstufa: 2, codigo: 2, modelo: 'Classic 500', idEstufa: 1 },
    { idModeloEstufa: 3, codigo: 3, modelo: 'Corner 640', idEstufa: 1 },
    { idModeloEstufa: 4, codigo: 4, modelo: 'Corner 650', idEstufa: 1 },
    { idModeloEstufa: 5, codigo: 5, modelo: 'Cubic 350', idEstufa: 1 },
    { idModeloEstufa: 6, codigo: 6, modelo: 'Cubic 380 Burdeo', idEstufa: 1 },
    { idModeloEstufa: 7, codigo: 7, modelo: 'Cubic 380 Charcoal', idEstufa: 1 },
    { idModeloEstufa: 8, codigo: 8, modelo: 'Firelog 400', idEstufa: 1 },
    { idModeloEstufa: 9, codigo: 9, modelo: 'NORDIC 350', idEstufa: 1 },
    { idModeloEstufa: 10, codigo: 10, modelo: 'NORDIC 360', idEstufa: 1 },
    { idModeloEstufa: 11, codigo: 11, modelo: 'NORDIC 380', idEstufa: 1 },
    { idModeloEstufa: 12, codigo: 12, modelo: 'NORDIC 450', idEstufa: 1 },
    { idModeloEstufa: 13, codigo: 13, modelo: 'RONDO 440', idEstufa: 1 },
    { idModeloEstufa: 14, codigo: 14, modelo: 'RONDO 450', idEstufa: 1 },
    { idModeloEstufa: 15, codigo: 15, modelo: 'RONDO 490', idEstufa: 1 },
    { idModeloEstufa: 16, codigo: 16, modelo: 'RONDO 440 DESING', idEstufa: 1 },
    { idModeloEstufa: 17, codigo: 17, modelo: 'RONDO 450 DESING', idEstufa: 1 },
    { idModeloEstufa: 18, codigo: 18, modelo: 'Scantek 350', idEstufa: 1 },
    { idModeloEstufa: 19, codigo: 19, modelo: 'Scantek 360', idEstufa: 1 },
    { idModeloEstufa: 20, codigo: 20, modelo: 'Scantek 380', idEstufa: 1 },
    { idModeloEstufa: 21, codigo: 21, modelo: 'Scantek 450', idEstufa: 1 },
    { idModeloEstufa: 22, codigo: 22, modelo: 'Eco 350', idEstufa: 2 },
    { idModeloEstufa: 23, codigo: 23, modelo: 'Limit 350', idEstufa: 2 },
    { idModeloEstufa: 24, codigo: 24, modelo: 'Multibosca 350', idEstufa: 2 },
    { idModeloEstufa: 25, codigo: 25, modelo: 'Limit 360', idEstufa: 2 },
    { idModeloEstufa: 26, codigo: 26, modelo: 'Eco 360', idEstufa: 2 },
    { idModeloEstufa: 27, codigo: 27, modelo: 'Limit 380', idEstufa: 2 },
    { idModeloEstufa: 28, codigo: 28, modelo: 'Spirit 380', idEstufa: 2 },
    { idModeloEstufa: 29, codigo: 29, modelo: 'ECO 380', idEstufa: 2 },
    { idModeloEstufa: 30, codigo: 30, modelo: 'ECO FLAME 360', idEstufa: 2 },
    { idModeloEstufa: 31, codigo: 31, modelo: 'X8 BASIC', idEstufa: 2 }
];