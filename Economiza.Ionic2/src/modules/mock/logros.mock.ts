import { ILogros } from '../../models/logros';

export const LogrosMock: ILogros[] = [
    { Id: 1, Titulo: 'Cuidado a la Salud infantil', Descripcion: 'Felicidades! Has logrado disminuir en XX% el riesgo de mortalidad no externa para infantes menores a 1 año!', Categoria: 'health', Logrado: true },
    { Id: 2, Titulo: 'Cuidado a la Salud en Enfermedades Crónicas', Descripcion: 'Felicidades! Has conseguido rebajar en un XX% el riesgo de mortalidad por causas cardiovasculares!', Categoria: 'health', Logrado: true },
    { Id: 3, Titulo: 'Cuidado a la Salud del Adulto Mayor', Descripcion: 'Tremendo! Lograste bajar en XX% el riesgo para personas mayores de 65 años de sufrir una hospitalización por causas respiratorias!', Categoria: 'life', Logrado: false },
    { Id: 4, Titulo: 'Ahorro en consumo', Descripcion: 'Felicitaciones! Has logrado economizar XX metros cúbicos de leña gracias a tu buena conducta!', Categoria: 'life', Logrado: true },
    { Id: 5, Titulo: 'Aire Limpio', Descripcion: 'Gran labor! Has logrado eliminar el XX% de un cigarro en la atmósfera!', Categoria: 'environment', Logrado: false }
];