import { IOrigen } from '../../models/origen';

export const OrigenMock: IOrigen[] = [
    { Id: 1,  Nombre: 'Hualle/Roble'},
    { Id: 2,  Nombre: 'Eucaliptus'},
    { Id: 3,  Nombre: 'Ulmo'},
];