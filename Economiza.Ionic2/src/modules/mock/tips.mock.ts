import { ITips } from '../../models/tips';

export const TipsMock: ITips[] = [
    { Id: 1, Codigo: 1, Titulo: 'Título 01', Descripcion: 'Preocúpese que la leña esté seca; ésta se reconoce porque los palos son más livianos, la corteza está semidesprendida y tiene grietas en los extremos' },
    { Id: 2, Codigo: 2, Titulo: 'Título 02', Descripcion: 'Asegúrese que le entreguen la leña cortada y medida en forma ordenada en el suelo. No permita la medición sobre el camión.' },
    { Id: 3, Codigo: 3, Titulo: 'Título 03', Descripcion: 'Pique su leña al tamaño definitivo, este no debe ser mayor a 12 cm de diámetro y almacénela en un lugar seco, ventilado y protegido de la lluvia y la humedad del suelo. ' },
    { Id: 4, Codigo: 4, Titulo: 'Título 04', Descripcion: 'Cargue su estufa regularmente cada 20 ó 40 minutos con 2 a 3 trozos de leña. Nunca llene la estufa de leña, sólo de esta manera tendrá una buena relación entre el combustible y el oxígeno de la combustión.' },
    { Id: 5, Codigo: 5, Titulo: 'Título 05', Descripcion: 'Compre su leña con anticipación, el mejor mes para hacerlo es septiembre y octubre, la leña está más barata y podrá picarla y secarla durante toda la primavera y verano, asegurándose de tener leña seca para el invierno' },
    { Id: 6, Codigo: 6, Titulo: 'Título 06', Descripcion: 'Limpie en cañón de su calefactor anualmente para evitar probables incendios.' },
    { Id: 7, Codigo: 7, Titulo: 'Título 07', Descripcion: 'No introduzca en su estufa papeles, plásticos u otros residuos. Todos ellos incrementan significativamente la contaminación de su combustión.' },
    { Id: 8, Codigo: 8, Titulo: 'Título 08', Descripcion: 'La leña debe almacenarse encastillada en lugar ventilado y sin contacto directo con el suelo. Mantener bajo techo en invierno.' },
    { Id: 9, Codigo: 9, Titulo: 'Título 09', Descripcion: 'Encender el fuego sólo con papel y astillas (no usar parafina, cera, basura, etc.).' },
    { Id: 10, Codigo: 10, Titulo: 'Título 10', Descripcion: 'La leña húmeda es menos eficiente, produce menos calor y contamina mucho más.' },
    { Id: 11, Codigo: 11, Titulo: 'Título 11', Descripcion: 'Ventile diariamente su casa.' },
    { Id: 12, Codigo: 12, Titulo: 'Título 12', Descripcion: 'Evite leñas de monte; su tala puede causar daños ecológicos irreparables.' },
    { Id: 13, Codigo: 13, Titulo: 'Título 13', Descripcion: 'Utilice leña seca, y mientras más dura, mejor es como combustible.' },
    { Id: 14, Codigo: 14, Titulo: 'Título 14', Descripcion: 'Mantén cualquier estufa lejos de cortinas, muebles o cualquier otro material inflamable.' },
    { Id: 15, Codigo: 15, Titulo: 'Título 15', Descripcion: 'No coloques ropa cerca o encima de la estufa.' },
    { Id: 16, Codigo: 16, Titulo: 'Título 16', Descripcion: 'No pongas recipientes con agua encima de la estufa.' },
    { Id: 17, Codigo: 17, Titulo: 'Título 17', Descripcion: 'Evita que los niños jueguen cerca de las estufas.' },
    { Id: 18, Codigo: 18, Titulo: 'Título 18', Descripcion: 'Compra y utiliza leña certificada. Si no sabes dónde comprar revisa nuestra base de datos.' },
    { Id: 19, Codigo: 19, Titulo: 'Título 19', Descripcion: 'Limpia las cenillas y el hollín constantemente.' },
    { Id: 20, Codigo: 20, Titulo: 'Título 20', Descripcion: 'Apaga el fuego siempre que salgas o te acuestes.' },
    { Id: 21, Codigo: 21, Titulo: 'Título 21', Descripcion: 'No cargues la estufa cuando el fuego esté muy intenso.' }
];