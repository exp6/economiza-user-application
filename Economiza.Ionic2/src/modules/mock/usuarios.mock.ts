import { IUsuario } from '../../models/Usuario';

export const UsuariosMock: IUsuario[] = [
    { Id: '1111', Email: 'juanito@indimin.cl', FotoPerfil: '', UserName: 'juanito123' },
    { Id: '222', Email: 'pepito@indimin.cl', FotoPerfil: '', UserName: '1q2w3e' }
];