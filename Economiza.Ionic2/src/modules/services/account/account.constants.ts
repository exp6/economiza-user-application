import { SERVICIO_API } from '../../../shared/baseurl.constants';

export class account_constants {

    // private
    private static AuthEndPoint: string = SERVICIO_API;
    private static ApiEndPoint: string = SERVICIO_API;

    // token

    public static readonly TokenName: string = 'access_token';

    //public static readonly Token_Endpoint: string = environment.AuthEndPoint + '/connect/token';
    public static readonly Token_Endpoint: string = account_constants.AuthEndPoint + '/token';

    //public static UserInfo_Endpoint: string = account_constants.AuthEndPoint + '/connect/userinfo';
    public static UserInfo_Endpoint: string = account_constants.AuthEndPoint + '/api/Account/UserInfo';

    public static readonly Client_Id: string = 'Angular';

    public static readonly Grant_Type_Password: string = 'password';

    public static readonly Grant_Type_RefreshToken: string = 'refresh_token';

    public static readonly Scope: string = 'api1 offline_access openid profile accountApi';

    // account

    public static Register_Endpoint: string = account_constants.AuthEndPoint + '/api/Account/Register';


    public static ResetPassword_Endpoint: string = account_constants.AuthEndPoint + '/api/Account/ResetPassword';

    public static ChangePassword_Endpoint: string = account_constants.AuthEndPoint + '/api/Account/ChangePassword';

    public static ForgotPassword_Endpoint: string = account_constants.AuthEndPoint + '/api/Account/ForgotPassword';

    // api

    public static NumbersApi_Endpoint: string = account_constants.ApiEndPoint + '/api/numbers';

}
