import { Injectable } from '@angular/core';
// import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

//import { AuthHttp } from 'angular2-jwt';
// import { AuthService } from './auth.service';
// import { ProfileService } from './profile.service';
import { TokenService } from './token.service';

import { account_constants } from './account.constants';

@Injectable()
export class AccountService {
    private headers: Headers;
    private options: RequestOptions;

    constructor(
        private http: Http,
        // private authService: AuthService,
        //private authHttp: AuthHttp, 
        // private profileService: ProfileService,
        private tokenService: TokenService
    ) {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    getToken(): string {
        let token: string = this.tokenService.getAccessToken(); //this.getAccessToken();
        return token;
    }

    public jsonBearerHeaders(): Headers {
        let headers: Headers = new Headers();
        //headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Content-Type', 'application/json');
        let token = this.getToken();
        if (token != null) {
            headers.append('Authorization', 'Bearer ' + token);
        }
        //console.log("jsonBearerHeaders", headers);
        return headers;
    }

    public register(username: string, displayName: string, password: string): Observable<any> {
        let requestBody = {
            Email: username,
            DisplayName: displayName,
            Password: password,
            ConfirmPassword: password
        }

        let _options = new RequestOptions({ headers: this.jsonBearerHeaders(), method: 'post' });
        return this.http.post(account_constants.Register_Endpoint, JSON.stringify(requestBody), _options)
            .map((res: Response) => {
                // console.log(res);
                // let responeBody = res.json();
                let obj: Object;

                // if (responeBody.succeeded) {
                if (res.status == 200 && res.statusText === 'OK') {
                    obj = {
                        code: 'Succeeded',
                        msg: 'Register complete'
                    }
                } else {
                    obj = {
                        code: res.statusText,
                        msg: res.toString()
                    }
                    // if (typeof responeBody.errors[0].code !== 'undefined') {
                    //     switch (responeBody.errors[0].code) {
                    //         case 'DuplicateUserName': {
                    //             obj = {
                    //                 code: 'DuplicateUserName',
                    //                 msg: 'Email already exist'
                    //             }
                    //             break;
                    //         }
                    //     }
                    // }
                    // console.log('responeBody', res);
                }

                return obj;
            })
            .catch(this.handleError);
    }

    public resetPassword(email: string, code: string, newPassword: string): Observable<any> {
        let requestBody = {
            Email: email,
            Password: newPassword,
            ConfirmPassword: newPassword,
            Code: code
        }
        let _options = new RequestOptions({ headers: this.jsonBearerHeaders(), method: 'post' });
        //return this.authHttp.post(account_constants.ResetPassword_Endpoint, JSON.stringify(requestBody), this.options)
        return this.http.post(account_constants.ResetPassword_Endpoint, JSON.stringify(requestBody), _options)
            .map((res: Response) => {
                let responeBody = res.json();
                let obj: Object;
                if (responeBody.Succeeded) {
                    obj = {
                        code: 'Succeeded',
                        msg: 'Change password complete'
                    }
                } else {
                    if (typeof responeBody.Errors[0].code !== 'undefined') {
                        switch (responeBody.Errors[0].code) {
                            case 'PasswordMismatch': {
                                obj = {
                                    code: 'PasswordMismatch',
                                    msg: 'Incorrect password'
                                }
                                break;
                            }
                        }
                    }
                }
                return obj;

            })
            .catch(this.handleError);
    }

    public changePassword(oldPassword: string, newPassword: string, confirmPassword: string): Observable<any> {
        let requestBody = {
            //Email: email, //this.profileService.getUserEmail(),
            OldPassword: oldPassword,
            NewPassword: newPassword,
            ConfirmPassword: confirmPassword
        }
        //console.log('changePassword', requestBody);
        let _options = new RequestOptions({ headers: this.jsonBearerHeaders(), method: 'post' });
        //return this.authHttp.post(account_constants.ChangePassword_Endpoint, JSON.stringify(requestBody), this.options)
        return this.http.post(account_constants.ChangePassword_Endpoint, JSON.stringify(requestBody), _options)
            .map((res: Response) => {
                // console.log('responeBody', res);
                // let responeBody = res.json();
                let obj: Object;
                // console.log('responeBody', responeBody);
                if (res.status == 200 && res.statusText === 'OK') {
                    obj = {
                        code: 'Succeeded',
                        msg: 'Change password complete'
                    }
                } else {
                    console.log('responeBody', res);
                    // if (typeof responeBody.errors[0].code !== 'undefined') {
                    //     switch (responeBody.errors[0].code) {
                    //         case 'PasswordMismatch': {
                    //             obj = {
                    //                 code: 'PasswordMismatch',
                    //                 msg: 'Incorrect password'
                    //             }
                    //             break;
                    //         }
                    //     }
                    // }
                }

                return obj;

            })
            .catch(this.handleError);
    }

    public forgotPassword(email: string, idioma: string): Observable<any> {
        let requestBody = {
            Email: email,
            idioma: idioma
        }
        let _options = new RequestOptions({ headers: this.jsonBearerHeaders(), method: 'post' });
        //console.log(this.options);
        //return this.authHttp.post(account_constants.ForgotPassword_Endpoint, JSON.stringify(requestBody), this.options)
        return this.http.post(account_constants.ForgotPassword_Endpoint, JSON.stringify(requestBody), _options)
            .map((res: Response) => {
                //console.log(res);
                let responeBody = res.status;
                let obj: Object;

                if (responeBody === 200) {
                    obj = {
                        code: 'Succeeded',
                        msg: 'Forgot password complete'
                    }
                } else {
                    obj = {
                        code: 'error',
                        msg: 'Forgot password error'
                    }
                }
                //console.log(obj);
                return obj;

            })
            .catch(this.handleError);
    }

    //
    // Helpers
    //

    private handleError(error: Response | any) {
        let errMsg: string;
        console.log('error', error);
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Observable.throw(errMsg);
    }
}
