import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
//import { Observable } from 'rxjs/Observable';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { ProfileService } from './profile.service';
import { TokenService } from './token.service';

import { account_constants } from './account.constants';

import { TransferHttp } from '../../../modules/transfer-http/transfer-http';

@Injectable()
export class AuthService {
    public isAuthenticatedBehaviorSubject = new BehaviorSubject<boolean>(this.tokenService.isTokenNotExpired());

    private header: Headers;
    private options: RequestOptions;
    private refreshSubscription: any;

    constructor(
        private http: Http,
        private profileService: ProfileService, private tokenService: TokenService, private transferHttp: TransferHttp) {
        this.header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new RequestOptions({ headers: this.header });
    }

    public isAuthenticated(): Observable<boolean> {
        //console.log('isAuthenticated');
        return this.isAuthenticatedBehaviorSubject.asObservable();
    }

    public singIn(username: string, password: string): Observable<any> {

        // identityserver4 openid spec
        let requestBody = new URLSearchParams();
        requestBody.append('client_id', account_constants.Client_Id);
        requestBody.append('grant_type', account_constants.Grant_Type_Password);
        requestBody.append('username', username);
        requestBody.append('password', password);
        requestBody.append('scope', account_constants.Scope);

        //console.log('singIn');
        // auth
        return this.http.post(account_constants.Token_Endpoint, requestBody, this.options)
            .map((res: Response) => {
                //console.log("res singIn", res);
                let responseBody: any = res.json();
                if (typeof responseBody.access_token !== 'undefined') {
                    this.saveToken(responseBody); // save token to localStorage
                    this.isAuthenticatedBehaviorSubject.next(true);
                    //console.log("getUserProfile singIn");
                    //if (this.tokenService.getAccessToken() != null && this.tokenService.getAccessToken() != '')
                    this.getUserProfile();
                }
            })
            .catch((error: any) => {
                //console.log("error singIn",error);
                let errorBody = error.json();
                return Observable.throw(errorBody.error_description);
            });
    }

    public signOut(): void {
        this.tokenService.removeToken();
        this.isAuthenticatedBehaviorSubject.next(false);
        this.unscheduleRefresh();
    }

    public unscheduleRefresh(): void {
        if (this.refreshSubscription) {
            this.refreshSubscription.unsubscribe();
        }
    }

    // private getNewJwt(): Observable<any> {

    //     // identityserver4 openid spec
    //     let requestBody = new URLSearchParams();
    //     requestBody.append('client_id', account_constants.Client_Id);
    //     requestBody.append('grant_type', account_constants.Grant_Type_RefreshToken);
    //     requestBody.append('refresh_token', this.tokenService.getRefreshToken());

    //     //console.log('getNewJwt');
    //     return this.http.post(account_constants.Token_Endpoint, requestBody, this.options)
    //         .map((res: Response) => {
    //             let responseBody: any = res.json();
    //             if (typeof responseBody.access_token !== 'undefined') {
    //                 this.saveToken(responseBody);

    //                 console.log('token refreshed');
    //             }
    //         })
    //         .catch((error: any) => {
    //             let errorBody = error.json();
    //             console.log('token refreshed error');

    //             return Observable.throw(errorBody.error);
    //         });
    // }

    private getUserProfile(): void {
        //console.log("before this.isAuthenticatedBehaviorSubject.getValue()");
        if (this.isAuthenticatedBehaviorSubject.getValue()) {
            //console.log("after this.isAuthenticatedBehaviorSubject.getValue()");
            //this.authHttp.get(account_constants.UserInfo_Endpoint)
            this.transferHttp.get(account_constants.UserInfo_Endpoint)
                .subscribe((res: Response) => {
                    //console.log("res getUserProfile", res);
                    //this.profileService.saveUserProfile(JSON.stringify(res.json()));
                    this.profileService.saveUserProfile(JSON.stringify(res));
                });
        }
    }

    //
    // helpers
    //

    private saveToken(response: any): void {
        console.log('response', response);
        this.tokenService.saveAcessToken(response.access_token);
        this.tokenService.saveExpiresIn(response.expires_in);
        this.tokenService.saveRefreshToken(response.refresh_token);
    }
}
