import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TransferHttp } from '../transfer-http/transfer-http';
import { SERVICIO_API } from '../../shared/baseurl.constants';

import { IEmail } from '../../models/email';


@Injectable()
export class EmailService {
    constructor(
        private transferHttp: TransferHttp, // Use only for GETS that you want re-used between Server render -> Client render
        // private http: Http // Use for everything else
    ) { }

    SendMensajeContacto(data: IEmail): Observable<boolean> {
        return this.transferHttp.post(`${SERVICIO_API}/ApiEmail/SendMensajeContacto`, data);
    }
}