import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { IGlosario } from '../../models/glosario';
import { GlosarioMock } from '../mock/glosario.mock';


@Injectable()
export class GlosarioService {
    constructor() { }

    getGlosario(): Observable<IGlosario[]> {
        return Observable.of(GlosarioMock);
    }
}
