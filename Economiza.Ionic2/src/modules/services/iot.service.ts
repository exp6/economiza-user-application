import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TransferHttp } from '../transfer-http/transfer-http';
import { SERVICIO_API } from '../../shared/baseurl.constants';
import { SERVICIO_API_CLOUD } from '../../shared/baseurl.constants';

import { IHistoriaUso, ILogro } from '../../models/iot';
// import { IMarcas } from '../../models/estufas';
// import { MarcasMock } from '../mock/estufas.mock';
// import { IModelos } from '../../models/estufas';
// import { ModelosMock } from '../mock/estufas.mock';


@Injectable()
export class IotService {
    constructor(
        private transferHttp: TransferHttp, // Use only for GETS that you want re-used between Server render -> Client render
        // private http: Http // Use for everything else
    ) { }

    ObtenerHistoria30Dias(channel: string): Observable<IHistoriaUso[]> {
        return this.transferHttp.get(`${SERVICIO_API}/ApiIot/ObtenerHistoria30Dias/${channel}`);
    }

    ObtenerLogrosUsuario(channel: string): Observable<ILogro[]> {
        return this.transferHttp.get(`${SERVICIO_API}/ApiIot/ObtenerLogrosUsuario/${channel}`);
    }

    ObtenerDiasUsoFiltro(channel: string): Observable<number> {
        return this.transferHttp.get(`${SERVICIO_API}/ApiIot/ObtenerDiasUsoFiltro/${channel}`);
    }

    ObtenerNotificacionMantencion(channel: string): Observable<string> {
        return this.transferHttp.get(`${SERVICIO_API_CLOUD}/ApiIot/ObtenerNotificacionMantencion/${channel}`);
    }

    RealizarMantencion(channel: string): Observable<number> {
        return this.transferHttp.get(`${SERVICIO_API_CLOUD}/ApiIot/RealizarMantencion/${channel}`);
    }

    ObtenerDiasFaltantesMantencion(channel: string): Observable<number> {
        return this.transferHttp.get(`${SERVICIO_API_CLOUD}/ApiIot/ObtenerDiasFaltantesMantencion/${channel}`);
    }

    // ObtenerJsonPubNub(subscribeKey: string, channel: string): Observable<any[]>{
    //     let now = Time.now;
    //     let timetoken = 
    //         let url = `https://ps.pndsn.com/subscribe/${subscribeKey}/${channel}/myJson/${timetoken}?uuid=pn-3bf77e4e-eba9-446d-84a5-c152ac99373d&heartbeat=300`;
    // }
}