import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TransferHttp } from '../transfer-http/transfer-http';
import { SERVICIO_API } from '../../shared/baseurl.constants';

import { IEstufa, IModeloEstufa } from '../../models/Listados';
// import { IMarcas } from '../../models/estufas';
// import { MarcasMock } from '../mock/estufas.mock';
// import { IModelos } from '../../models/estufas';
// import { ModelosMock } from '../mock/estufas.mock';


@Injectable()
export class ListadosService {
    constructor(
        private transferHttp: TransferHttp, // Use only for GETS that you want re-used between Server render -> Client render
        // private http: Http // Use for everything else
    ) { }

    ObtenerMarcasEstufas(): Observable<IEstufa[]> {
        // return Observable.of(MarcasMock);
        return this.transferHttp.get(`${SERVICIO_API}/ApiListados/ObtenerMarcasEstufas`);
    }

    ObtenerModelosEstufas(idEstufa: number): Observable<IModeloEstufa[]> {
        // return Observable.of(ModelosMock.filter(x => x.idEstufa == idEstufa));
        return this.transferHttp.get(`${SERVICIO_API}/ApiListados/ObtenerModelosEstufas/${idEstufa}`);
    }
}