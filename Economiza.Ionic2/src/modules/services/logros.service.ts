import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ILogros } from '../../models/logros';
import { LogrosMock } from '../mock/logros.mock';


@Injectable()
export class LogrosService {
    constructor() { }

    getLogros(): Observable<ILogros[]> {
        return Observable.of(LogrosMock);
    }
}
