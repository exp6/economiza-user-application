import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { IOrigen } from '../../models/origen';
import { OrigenMock } from '../mock/origen.mock';


@Injectable()
export class OrigenService {
    constructor() { }

    getOrigen(): Observable<IOrigen[]> {
        return Observable.of(OrigenMock);
    }
}
