import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
//import { Observable } from 'rxjs/Observable';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class ParameterService {
    public fireStatusBehaviorSubject = new BehaviorSubject<boolean>(false);

    private fireStatus: boolean;

    constructor() {
    }

    public getFiredStatus(): Observable<boolean> {
        return this.fireStatusBehaviorSubject.asObservable();
    }

    public setFiredStatus(firedstatus: number) {
        console.log('ParameterService_ParameterService_ParameterService', firedstatus);
        if (firedstatus == 0)
            this.fireStatusBehaviorSubject.next(true);
        else
            this.fireStatusBehaviorSubject.next(false);
    }
}
