import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TransferHttp } from '../transfer-http/transfer-http';
import { SERVICIO_API } from '../../shared/baseurl.constants';

import { ITips } from '../../models/tips';
// import { TipsMock } from '../mock/tips.mock';


@Injectable()
export class TipsService {
    public maxTips: any;
    constructor(
        private transferHttp: TransferHttp, // Use only for GETS that you want re-used between Server render -> Client render
        // private http: Http // Use for everything else
    ) { }

    getTips(cantidad: number): Observable<ITips[]> {
        // if(cantidad) {  
        //     this.maxTips = Observable.of(TipsMock.slice(0,cantidad));
        // } else {
        //     this.maxTips = Observable.of(TipsMock);
        // }
        // return this.maxTips;
        if (!cantidad)
            cantidad = 0;
        return this.transferHttp.get(`${SERVICIO_API}/ApiListados/ObtenerTips/${cantidad}`);
    }
}