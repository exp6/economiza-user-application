// import { Injectable, Inject } from '@angular/core';
import { Injectable } from '@angular/core';
// import { Http, URLSearchParams } from '@angular/http';
// import { Http } from '@angular/http';
// import { APP_BASE_HREF } from '@angular/common';

import { Observable } from 'rxjs/Observable';

import { SERVICIO_API } from '../../shared/baseurl.constants';
import { IUsuario } from '../../models/Usuario';
import { IPerfil } from '../../models/Perfil';
import { UsuariosMock } from '../mock/usuarios.mock';
import 'rxjs/add/observable/of';
import { TransferHttp } from '../transfer-http/transfer-http';

@Injectable()
export class UsuarioService {
    constructor(
        private transferHttp: TransferHttp, // Use only for GETS that you want re-used between Server render -> Client render
        // private http: Http // Use for everything else
    ) { }

    getUsuarioConectado(): Observable<IUsuario> {
        return this.transferHttp.get(`${SERVICIO_API}/ApiUsuarios/ObtenerUsuario`)
            .catch(e => {
                return Observable.of(new Usuario(e.status, 'error_' + e.status, e, '')).map(o => o); //.map(o => JSON.stringify(o));
            });
    }

    getUsuario(): Observable<IUsuario> {
        return this.transferHttp.get(`${SERVICIO_API}/ApiUsuarios/ObtenerUsuario`);
    }

    getUsuarios(): Observable<IUsuario[]> {
        return Observable.of(UsuariosMock);
    }

    getPerfil(): Observable<IPerfil> {
        return this.transferHttp.get(`${SERVICIO_API}/api/Account/UserInfoExternal`).catch(e => {
            return Observable.of(new Perfil(e.status, 'error_' + e.status, false, e, '', '', '', '', '', 0, 0, 0, 0, 0)).map(o => o); //.map(o => JSON.stringify(o));
        });
    }

    updatePerfil(data: IPerfil): Observable<boolean> {
        return this.transferHttp.post(`${SERVICIO_API}/ApiUsuarios/UpdatePerfil`, data);
    }
}

class Usuario implements IUsuario {

    constructor(public Id, public Email, public FotoPerfil, public UserName) { }
}

class Perfil implements IPerfil {

    constructor(
        public Id,
        public Email,
        public HasRegistered,
        public LoginProvider,
        public DisplayName,
        public Picture,
        public telefono,
        public nroKit,
        public direccion,
        public idEstufa,
        public idModeloEstufa,
        public nroCohabitantes,
        public cantPromM3,
        public cantPromMeses
    ) { }
}