// import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { Injectable } from '@angular/core';
// import { ConnectionBackend, Http, Headers, Request, RequestOptions, RequestOptionsArgs, Response } from '@angular/http';
import { Http, Headers, Request, RequestOptions, RequestOptionsArgs, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// import { Subject } from 'rxjs/Subject';
// import { isPlatformServer } from '@angular/common';

import { TokenService } from '../services/account/token.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/fromPromise';

@Injectable()
export class TransferHttp {

    // private isServer = isPlatformServer(this.platformId);

    private header: Headers;
    private options: RequestOptions;

    constructor(
        // @Inject(PLATFORM_ID) private platformId,
        private http: Http,
        private tokenService: TokenService
    ) {
        let token = this.getToken();
        if (token != null) {
            //console.log('this.getToken()', token);
            this.header = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + token
            });
            this.options = new RequestOptions({ headers: this.header });
        } else {
            //console.log('this.getToken()', null);
            this.header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
            this.options = new RequestOptions({ headers: this.header });
        }
    }

    getToken(): string {
        let token: string = this.tokenService.getAccessToken(); //this.getAccessToken();
        return token;
    }

    public jsonBearerHeaders(): Headers {
        let headers: Headers = new Headers();
        //headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Content-Type', 'application/json');
        let token = this.getToken();
        if (token != null) {
            headers.append('Authorization', 'Bearer ' + token);
        }
        //console.log("jsonBearerHeaders", headers);
        return headers;
    }

    request(uri: string | Request, options?: RequestOptionsArgs): Observable<any> {
        return this.getData(uri, options, (url: string, options: RequestOptionsArgs) => {
            return this.http.request(url, options);
        });
    }
    /**
     * Performs a request with `get` http method.
     */
    get(url: string, options?: RequestOptionsArgs): Observable<any> {
        /*this.header = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this.getToken()
        });
        this.options = new RequestOptions({ headers: this.header });*/
        options = this.options;
        return this.getData(url, options, (url: string, options: RequestOptionsArgs) => {
            let _options = new RequestOptions({ headers: this.jsonBearerHeaders(), method: 'get' });
            //console.log("optionsgetData", options);
            return this.http.get(url, _options);
        });
    }
    /**
     * Performs a request with `post` http method.
     */
    post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        options = this.options;
        let _body = body;
        return this.getPostData(url, body, options, (url: string, options: RequestOptionsArgs) => {
            let _options = new RequestOptions({ headers: this.jsonBearerHeaders(), method: 'post' });
            //console.log(_body);
            return this.http.post(url, _body, _options);
        });
    }
    /**
     * Performs a request with `put` http method.
     */
    put(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        options = this.options;
        let _body = body;
        return this.getPostData(url, body, options, (url: string, options: RequestOptionsArgs) => {
            let _options = new RequestOptions({ headers: this.jsonBearerHeaders(), method: 'put' });
            return this.http.put(url, _body, _options);
        });
    }
    /**
     * Performs a request with `delete` http method.
     */
    delete(url: string, options?: RequestOptionsArgs): Observable<any> {
        options = this.options;
        return this.getData(url, options, (url: string, options: RequestOptionsArgs) => {
            return this.http.delete(url, options);
        });
    }
    /**
     * Performs a request with `patch` http method.
     */
    patch(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        options = this.options;
        return this.getPostData(url, body, options, (url: string, options: RequestOptionsArgs) => {
            let _options = new RequestOptions({ headers: this.jsonBearerHeaders(), method: 'patch' });
            return this.http.patch(url, body, _options);
        });
    }
    /**
     * Performs a request with `head` http method.
     */
    head(url: string, options?: RequestOptionsArgs): Observable<any> {
        options = this.options;
        return this.getData(url, options, (url: string, options: RequestOptionsArgs) => {
            return this.http.head(url, options);
        });
    }
    /**
     * Performs a request with `options` http method.
     */
    requestoptions(url: string, options?: RequestOptionsArgs): Observable<any> {
        options = this.options;
        return this.getData(url, options, (url: string, options: RequestOptionsArgs) => {
            return this.http.options(url, options);
        });
    }

    private getData(uri: string | Request, options: RequestOptionsArgs, callback: (uri: string | Request, options?: RequestOptionsArgs) => Observable<Response>) {
        let url = uri;
        if (typeof uri !== 'string') {
            url = uri.url;
        }
        // const key = url + JSON.stringify(options);
        return callback(url, options)
            .map(res => res.json())
            .do(data => {
                // if (this.isServer) {
                //     this.setCache(key, data);
                // }
            });
    }

    private getPostData(uri: string | Request, body: any, options: RequestOptionsArgs, callback: (uri: string | Request, body: any, options?: RequestOptionsArgs) => Observable<Response>) {

        let url = uri;

        if (typeof uri !== 'string') {
            url = uri.url;
        }

        // const key = url + JSON.stringify(body);

        return callback(uri, body, options)
            .map(res => res.json())
            .do(data => {
                // if (this.isServer) {
                //     this.setCache(key, data);
                // }
            });
    }

    // private resolveData(key: string) {
    //     const data = this.getFromCache(key);

    //     if (!data) {
    //         throw new Error();
    //     }

    //     return Observable.fromPromise(Promise.resolve(data));
    // }

    // private setCache(key, data) {
    //     return this.transferState.set(key, data);
    // }

    // private getFromCache(key): any {
    //     return this.transferState.get(key);
    // }
}
