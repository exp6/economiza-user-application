import { Component } from '@angular/core';
import { Platform, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform) {

  }

  ionViewDidLoad() {

  }

  launch(url) {
      this.platform.ready().then(() => {
        let browser = new InAppBrowser(url, "_system");
      });
    }
}
