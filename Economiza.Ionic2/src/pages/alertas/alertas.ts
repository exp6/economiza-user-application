import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-alertas',
  templateUrl: 'alertas.html',
})
export class AlertasPage {

  constructor(public alerCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlertasPage');
  }

  showAlert() {
    let alert = this.alerCtrl.create({
      message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in pellentesque nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.',
      buttons: ['Aceptar']
    });
    alert.present()
  }
}
