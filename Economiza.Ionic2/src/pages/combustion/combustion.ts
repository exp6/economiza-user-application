import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { PubNubAngular } from 'pubnub-angular2';
import { ReportesPage } from '../reportes/reportes';

import { IPerfil } from '../../models/Perfil';
import { UsuarioService } from '../../modules/services/usuario.service';

@Component({
  selector: 'page-combustion',
  templateUrl: 'combustion.html',
})
export class CombustionPage implements OnInit {

  public perfil: IPerfil;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    pubnubService: PubNubAngular,
    public usuarioService: UsuarioService
  ) {
    /*
    publish-key: pub-c-d3b0ade2-ec5a-4c6d-8636-d2d0854b425f
    subscribe-key: sub-c-f570d25c-8993-11e7-9760-3a607be72b06
    */


    // pubnubService.init({
    //   publish_key: 'pub-c-a3c37225-35ff-480d-9375-fc3722d2116c',
    //   subscribe_key: 'sub-c-c7207e20-513f-11e7-a368-0619f8945a4f',
    // });

    // pubnubService.publish(
    //   {
    //     message: { such: 'Hello!' },
    //     channel: 'awesomeChannel'
    //   },
    //   (status, response) => {
    //     if (status.error) {
    //       console.log(status);
    //     } else {
    //       console.log('message Published w/ timetoken', response.timetoken);
    //     }
    //   });
    // let jsEon = `
    // var pubnub = new PubNub({
    //             publishKey: 'pub-c-a3c37225-35ff-480d-9375-fc3722d2116c', // replace with your own pub-key
    //             subscribeKey: 'sub-c-c7207e20-513f-11e7-a368-0619f8945a4f' // replace with your own sub-key
    //         });

    // eon.chart({
    //   pubnub: pubnub,
    //   channels: ["awesomeChannel"],
    //   generate: {
    //     bindto: '#chart',
    //     data: {
    //       type: "gauge",
    //     },
    //     gauge: {
    //       min: 0,
    //       max: 100
    //     },
    //     color: {
    //       pattern: ['#FF0000', '#F6C600', '#60B044'],
    //       threshold: {
    //         values: [30, 60, 90]
    //       }
    //     }
    //   },
    //   transform: function (data) {
    //     console.log("Oxygen value: "+ data);
    //     return {
    //       eon: {
    //         'data': parseFloat(data)
    //       }
    //     };
    //   }
    // });`;

    // eon.chart({
    //   pubnub: pubnub,
    //   channels: ["265001"],
    //   generate: {
    //     bindto: '#chartCombustion',
    //     data: {
    //       type: "gauge",
    //     },
    //     gauge: {
    //       min: 0,
    //       max: 100
    //     },
    //     color: {
    //       pattern: ['#FF0000', '#F6C600', '#60B044'],
    //       threshold: {
    //         values: [30, 60, 90]
    //       }
    //     }
    //   },
    //   transform: function (message) {
    //     var dataMsg = JSON.parse(message);
    //     console.log("temperature value: ", dataMsg);
    //     return {
    //       eon: {
    //         'data': parseFloat(dataMsg.temperature)
    //       }
    //     };
    //   }
    // });


    // eon.chart({
    //   pubnub: pubnub,
    //   channels: ["265001"],
    //   generate: {
    //     bindto: '#chartCO_CF',
    //     data: {
    //       type: "gauge",
    //     },
    //     gauge: {
    //       min: 0,
    //       max: 100
    //     },
    //     color: {
    //       pattern: ['#FF0000', '#F6C600', '#60B044'],
    //       threshold: {
    //         values: [30, 60, 90]
    //       }
    //     }
    //   },
    //   transform: function(message) {
    //     var dataMsg = JSON.parse(message);
    //     console.log("CO Con Filtro value: " + dataMsg.co_ppm_filter);
    //     return {
    //       eon: {
    //         'data': parseFloat(dataMsg.co_ppm_filter)
    //       }
    //     };
    //   }
    // });

    // eon.chart({
    //   pubnub: pubnub,
    //   channels: ["265001"],
    //   generate: {
    //     bindto: '#chartMP_CF',
    //     data: {
    //       type: "gauge",
    //     },
    //     gauge: {
    //       min: 0,
    //       max: 100
    //     },
    //     color: {
    //       pattern: ['#FF0000', '#F6C600', '#60B044'],
    //       threshold: {
    //         values: [30, 60, 90]
    //       }
    //     }
    //   },
    //   transform: function(message) {
    //     var dataMsg = JSON.parse(message);
    //     console.log("MP Con Filtro value: " + dataMsg.co_mp_mg_m3_filter);
    //     return {
    //       eon: {
    //         'data': parseFloat(dataMsg.co_mp_mg_m3_filter)
    //       }
    //     };
    //   }
    // });

    // eon.chart({
    //   pubnub: pubnub,
    //   channels: ["265001"],
    //   generate: {
    //     bindto: '#chartCO_SF',
    //     data: {
    //       type: "gauge",
    //     },
    //     gauge: {
    //       min: 0,
    //       max: 100
    //     },
    //     color: {
    //       pattern: ['#FF0000', '#F6C600', '#60B044'],
    //       threshold: {
    //         values: [30, 60, 90]
    //       }
    //     }
    //   },
    //   transform: function(message) {
    //     var dataMsg = JSON.parse(message);
    //     console.log("CO Sin Filtro value: " + dataMsg.co_ppm);
    //     return {
    //       eon: {
    //         'data': parseFloat(dataMsg.co_ppm)
    //       }
    //     };
    //   }
    // });

    // eon.chart({
    //   pubnub: pubnub,
    //   channels: ["265001"],
    //   generate: {
    //     bindto: '#chartMP_SF',
    //     data: {
    //       type: "gauge",
    //     },
    //     gauge: {
    //       min: 0,
    //       max: 100
    //     },
    //     color: {
    //       pattern: ['#FF0000', '#F6C600', '#60B044'],
    //       threshold: {
    //         values: [30, 60, 90]
    //       }
    //     }
    //   },
    //   transform: function(message) {
    //     var dataMsg = JSON.parse(message);
    //     console.log("MP Sin Filtro value: " + dataMsg.co_mp_mg_m3);
    //     return {
    //       eon: {
    //         'data': parseFloat(dataMsg.co_mp_mg_m3)
    //       }
    //     };
    //   }
    // });
    // `;

    // {
    //   "deviceid": "air-device-001", // -------> ID dispositivo
    //     "num_kit": 265007, // -------> Canal registrado por usuario
    //       "id_estufa_modelo": 6, // -------> ID modelo estufa de BD
    //         "temperature": 100.8, // -------> Temperatura Celsius
    //           "o2": 14.0, // ------->  % Oxigeno
    //             "co_ppm_filter": 233.2719298, // -------> CO con filtro
    //               "co_ppm": 849.8, // -------> CO sin filtro
    //                 "co_mp_mg_m3_filter": 612.5461375, //  -------> MP con filtro
    //                   "co_mp_mg_m3": 1753.767852, // -------> MP sin filtro
    //                     "firestatus": "0", // -------> 1: Buena combustion , 0: Mala combustion 
    //                       "oxygenmpfix": 65.072564376097361, // ---------> % Correccion MP con filtro versus sin filtro
    //                         "oxygencofix": 72.5497846758577, // ---------> % Correccion CO con filtro versus sin filtro
    //                           "airexcesspercentage": 211.82690788405881 // --------------> % Exceso de aire
    // }

    // let jsEonTestGauge = `
    // var pubnub = new PubNub({
    //   publishKey: 'demo',
    //   subscribeKey: 'demo'
    // });
    // eon.chart({
    //   pubnub: pubnub,
    //   channels: ['eon-gauge'],
    //   generate: {
    //     bindto: '#chartTestGauge',
    //     data: {
    //       type: 'gauge',
    //     },
    //     gauge: {
    //       min: 0,
    //       max: 100
    //     },
    //     color: {
    //       pattern: ['#FF0000', '#F6C600', '#60B044'],
    //       threshold: {
    //         values: [30, 60, 90]
    //       }
    //     }
    //   }
    // });

    // setInterval(function(){
    //   pubnub.publish({
    //     channel: 'eon-gauge',
    //     message: {
    //       eon: {
    //         'data': Math.random() * 99
    //       }
    //     }
    //   })

    // }, 1000);
    // `;

    // let jsEonTestSPLine = `
    // var __eon_pubnub = new PubNub({
    //   subscribeKey: "sub-c-bd9ab0d6-6e02-11e5-8d3b-0619f8945a4f"
    // });
    // var __eon_cols = ["Austin","New York","San Francisco","Portland"]; 
    // var __eon_labels = {};  
    // eon.chart({
    //   pubnub: __eon_pubnub,
    //   channels: ["test-channel-0.3409516635847176"],
    //   history: false,
    //   flow: true,
    //   rate: 1000,
    //   limit: 5,
    //   generate: {
    //     bindto: "#chartTestSPLine",
    //     data: {
    //       colors: {"Austin":"#D70060","New York":"#E54028","San Francisco":"#F18D05","Portland":"#113F8C"},
    //       type: "spline"
    //     },
    //     transition: {
    //       duration: 250
    //     },
    //     axis: {
    //       x: {
    //         label: ""
    //       },
    //       y: {
    //         label: ""
    //       }
    //     },
    //     grid: {
    //       x: {
    //         show: false 
    //       },
    //       y: {
    //         show: false 
    //       }
    //     },
    //     tooltip: {
    //     show: true
    //     },
    //     point: {
    //       show: true
    //     }
    //   },
    //   transform: function(message) {
    //     var message = eon.c.flatten(message.eon);
    //     var o = {};
    //     for(var index in message) {
    //       if(__eon_cols.indexOf(index) > -1){
    //         o[__eon_labels[index] || index] = message[index];
    //       }
    //     }
    //     return {
    //       eon: o
    //     };
    //   }
    // });
    // `;

  }

  ngOnInit(): void {
    this.usuarioService.getPerfil().subscribe(result => {
      // console.log('result', result);
      this.perfil = result as IPerfil;
      this.cargarGraficos();
    });
  }

  cargarGraficos(): void {
    let jsEonChartCombustion = `

    /**
     * Number.prototype.format(n, x, s, c)
     * 
     * @param integer n: length of decimal
     * @param integer x: length of whole part
     * @param mixed   s: sections delimiter
     * @param mixed   c: decimal delimiter
     */
    Number.prototype.format = function(n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));
        
        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };

    var pubnub = new PubNub({
                publishKey: 'pub-c-56a38c54-26de-4c6a-99d2-9cb7b7e3db29', // replace with your own pub-key
                subscribeKey: 'sub-c-cda52064-6758-11e7-9bf2-0619f8945a4f' // replace with your own sub-key
            });

    
    
    eon.chart({
      pubnub: pubnub,
      channels: ["${this.perfil.nroKit}"],
      generate: {
        bindto: '#chartOxigeno',
        data: {
          type: "gauge",
        },
        gauge: {
          min: 8,
          max: 21
        },
        color: {
          pattern: ['#F6C600', '#60B044', '#FF0000'],
          threshold: {
            values: [10, 15, 70]
          }
        }
      },
      transform: function(message) {
        var dataMsg = JSON.parse(message);
        console.log("O value: " + dataMsg.o2);
        
        var msgCalidadCombustion = 'Mala';
        var iconCalidadCombustion = 'thumbs-down';
        if(dataMsg.firestatus == 1){
          var msgCalidadCombustion = 'Buena';
        var iconCalidadCombustion = 'thumbs-up';
        }
        document.getElementById("msgCalidadCombustion").innerHTML = msgCalidadCombustion;
        document.getElementById("iconCalidadCombustion").setAttribute("name", iconCalidadCombustion);
        document.getElementById("iconCalidadCombustion").setAttribute("class", "icon icon-ios ion-ios-" + iconCalidadCombustion);

        document.getElementById("chartCO_CF").innerHTML = dataMsg.co_ppm_filter.format(2, 3, '.', ',');
        document.getElementById("chartCO_SF").innerHTML = dataMsg.co_ppm.format(2, 3, '.', ',');
        document.getElementById("chartMP_CF").innerHTML = dataMsg.co_mp_mg_m3_filter.format(2, 3, '.', ',');
        document.getElementById("chartMP_SF").innerHTML = dataMsg.co_mp_mg_m3.format(2, 3, '.', ',');
        return {
          eon: {
            'data': parseFloat(dataMsg.o2)
          }
        };
      }
    });
    `;

    setTimeout(function () {
      eval(jsEonChartCombustion);
    }, 50);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CombustionPage');
  }
  goReportes() {
    this.navCtrl.push(ReportesPage);
  }
}