import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CANT_TIPS_INICIO } from '../../shared/constants/general.constants';

import { TipsPage } from '../tips/tips';

import { IOrigen } from '../../models/origen';
import { OrigenService } from '../../modules/services/origen.service';

@Component({
  selector: 'page-compras',
  templateUrl: 'compras.html',
})
export class ComprasPage {

  public origen: IOrigen[];

  public showBtMenu: boolean = false;
  public showBtHome: boolean = false;
  public compras_title: string = 'Ingresar primera compra';
  public humedad: number = 40;
  public blokHumedad: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private origenService: OrigenService) {
  }

  ionViewDidLoad() {
    this.getOrigen();
    console.log('ionViewDidLoad ComprasPage');
  }

  goTips() {
    this.navCtrl.setRoot(TipsPage, { 'cantTips': CANT_TIPS_INICIO });
  }

  getOrigen(): void {
    this.origenService.getOrigen().subscribe(result => {
      this.origen = result as IOrigen[];
    });
  }
}