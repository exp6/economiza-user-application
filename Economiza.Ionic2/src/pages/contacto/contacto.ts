import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';

import { EmailService } from '../../modules/services/email.service';
import { IEmail } from '../../models/email';

export class EmailModel {
  constructor(
    public asunto: string,
    public mensaje: string,
  ) { }
}

@Component({
  selector: 'page-contacto',
  templateUrl: 'contacto.html',
})
export class ContactoPage implements OnInit {
  public showForm: boolean = true;

  public mostrarForm: boolean = false;
  public loading: boolean = false;
  public actionError: boolean;
  public actionErrorMsg: string;
  public returnUrl: string;
  public emailModel: EmailModel;
  public emailForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private emailService: EmailService,
    private fb: FormBuilder,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactoPage');
  }

  ngOnInit(): void {
    this.cargarForm();
  }

  cargarForm() {
    this.emailModel = new EmailModel('', '');
    this.buildEmailForm();
  }

  public buildEmailForm(): void {
    this.emailForm = this.fb.group({
      asunto: [this.emailModel.asunto, [
        Validators.required]
      ],
      mensaje: [this.emailModel.mensaje, Validators.required]
    });

    this.emailForm.valueChanges.subscribe(data => this.onEmailValueChanged(data));
  }

  public onEmailValueChanged(data?: any) {
    // console.log('onLoginValueChanged', data);
    if (!this.emailForm) {
      return;
    }
    const form = this.emailForm;
    // console.log('onLoginValueChanged: form', form);
    for (const field in this.formEmailErrors) {
      // clear previous error message (if any)
      //console.log('onLoginValueChanged: field', field);
      this.formEmailErrors[field] = '';
      const control = form.get(field);
      // console.log('onLoginValueChanged: control', control);
      // console.log('onLoginValueChanged: pristine', control.pristine);
      // console.log('onLoginValueChanged: dirty', control.dirty);
      // console.log('onLoginValueChanged: valid', control.valid);
      // if (control && control.dirty && !control.valid) {
      if (control && !control.valid) {
        //if (control && !control.valid) {
        const messages = this.validationMessages[field];
        // console.log('onLoginValueChanged: messages', messages);
        for (const key in control.errors) {
          // console.log('onLoginValueChanged: key', key);
          this.formEmailErrors[field] += messages[key] + '<br />';
        }
      }
    }
    // console.log("formLoginErrors", this.formLoginErrors);
  }

  public formEmailErrors = {
    'asunto': '',
    'mensaje': ''
  }

  public validationMessages = {
    'asunto': {
      'required': 'El asunto es requerido'
    },
    'mensaje': {
      'required': 'El mensaje es requerido'
    }
  }

  // sendForm() {
  //   this.showForm = false;
  // }

  public onSubmitEmail(): void {
    this.loading = true;

    let dataPost: IEmail = {
      emailDestino: "",
      asunto: this.emailForm.value.asunto,
      mensaje: this.emailForm.value.mensaje,
    }

    this.emailService.SendMensajeContacto(dataPost)
      .subscribe(result => {
        this.actionError = true; // show the error

        if (result) {
          this.actionErrorMsg = 'Email enviado correctamente';
        }
        else {
          this.actionErrorMsg = 'Problemas al enviar email';
        }
        this.loading = false;
      },
      error => {
        this.loading = false;
      });
  }
}