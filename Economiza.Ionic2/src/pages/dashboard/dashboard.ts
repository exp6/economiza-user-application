import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LogrosPage } from '../logros/logros';
import { MantencionPage } from '../mantencion/mantencion';
import { DatosPage } from '../datos/datos';

import { IPerfil } from '../../models/Perfil';
import { UsuarioService } from '../../modules/services/usuario.service';
import { IotService } from '../../modules/services/iot.service';

import { ParameterService } from '../../modules/services/parameters-behaviors';

import { PubNubAngular } from 'pubnub-angular2';

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements OnInit {
  iconLevel: string = "leaf";

  public perfil: IPerfil;
  public dias_uso_filtro: number;
  public tMantencion: string = "";
  public tTest: number = 28;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public usuarioService: UsuarioService,
    public iotService: IotService,
    private parameterService: ParameterService,
    public pubnubService: PubNubAngular
  ) {

  }

  cargarGraficos(): void {
    //console.log('cargarGraficos');
    let jsEonChartCombustion = `
    var pubnub = new PubNub({
                publishKey: 'pub-c-56a38c54-26de-4c6a-99d2-9cb7b7e3db29', // replace with your own pub-key
                subscribeKey: 'sub-c-cda52064-6758-11e7-9bf2-0619f8945a4f' // replace with your own sub-key
            });

    eon.chart({
      pubnub: pubnub,
      channels: ["${this.perfil.nroKit}"],
      generate: {
        bindto: '#chartCombustionHome',
        data: {
          type: "gauge",
        },
        gauge: {
          min: 0,
          max: 180
        },
        color: {
          pattern: ['#F6C600', '#60B044', '#FF0000'],
          threshold: {
            values: [30, 60, 90]
          }
        }
      },
      transform: function(message) {
        var dataMsg = JSON.parse(message);
        //console.log("temperature value: ", dataMsg);

        var msgCalidadCombustion = 'Mala';
        var iconCalidadCombustion = 'thumbs-down';
        if(dataMsg.firestatus == 1){
          var msgCalidadCombustion = 'Buena';
        var iconCalidadCombustion = 'thumbs-up';
        }

        //this.setFiredStatus(dataMsg.firestatus);

        document.getElementById("msgCalidadCombustion").innerHTML = msgCalidadCombustion;
        document.getElementById("iconCalidadCombustion").setAttribute("name", iconCalidadCombustion);
        document.getElementById("iconCalidadCombustion").setAttribute("class", "icon icon-ios ion-ios-" + iconCalidadCombustion);

        return {
          eon: {
            'data': parseFloat(dataMsg.temperature)
          }
        };
      }
    });
    `;

    setTimeout(function () {
      eval(jsEonChartCombustion);
    }, 1500);
  }

  ngOnInit(): void {
    this.usuarioService.getPerfil().subscribe(result => {
      // console.log('result', result);
      this.perfil = result as IPerfil;
      this.cargarDiasUso(this.perfil.nroKit);
      this.cargarGraficos();

      this.iotService.ObtenerDiasFaltantesMantencion(this.perfil.nroKit).subscribe(result => {
        console.log('result', result);
        this.tTest = result as number;
        this.tMantencion = (7-this.tTest)+"";
        // this.notificacionMantencion = result as string;
      });
    });

    
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad DashboardPage');
  }



  cargarDiasUso(channel) {
    this.iotService.ObtenerDiasUsoFiltro(channel).subscribe(result => {
      // console.log('result', result);
      this.dias_uso_filtro = result as number;
    });
  }

  goLogros() {
    this.navCtrl.push(LogrosPage);
  }
  goMantencion() {
    this.navCtrl.push(MantencionPage);
  }
  goDatos() {
    this.navCtrl.push(DatosPage);
  }
}