import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';

import { ComprasPage } from '../compras/compras';

// import { IMarcas } from '../../models/estufas';
// import { MarcasService } from '../../modules/services/estufas.service';
// import { IModelos } from '../../models/estufas';
// import { IUsuario } from '../../models/Usuario';
import { IEstufa, IModeloEstufa } from '../../models/Listados';
import { IPerfil } from '../../models/Perfil';
import { ListadosService } from '../../modules/services/listados.service';
import { UsuarioService } from '../../modules/services/usuario.service';

import { DashboardPage } from '../dashboard/dashboard';

export class PerfilModel {
  constructor(
    public email: string,
    public displayName: string,
    public telefono: string,
    public nroKit: string,
    public direccion: string,
    public idMarcaEstufa: number,
    public selectMarcaEstufa: number,
    public idModeloEstufa: number,
    public selectModeloEstufa: number,
    public nroCohabitantes: number,
    public cantPromM3: number,
    public cantPromMeses: number,
    public selectCantPromMeses: number
  ) { }
}

@Component({
  selector: 'page-datos',
  templateUrl: 'datos.html',
})
export class DatosPage implements OnInit {
  public loadingReg: boolean = false;
  public actionError: boolean;
  public actionErrorMsg: string;
  public perfilModel: PerfilModel;
  public perfilForm: FormGroup;

  public perfil: IPerfil;

  public estufas: IEstufa[];
  public modelosEstufas: IModeloEstufa[];
  public showBtMenu: boolean = false;
  public showBtHome: boolean = false;
  public blokModel: boolean = true;

  public idEstufa: number;
  public idModeloEstufa: number;
  public cantPromMeses: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private listadosService: ListadosService,
    private usuarioService: UsuarioService,
    private fb: FormBuilder) {
  }
  // ionViewWillEnter() {
  //   this.viewCtrl.showBackButton(false);
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DatosPage');
  }

  ngOnInit(): void {
    this.perfilModel = new PerfilModel('', '', '', '', '', 0, null, 0, null, null, null, 1, null);
    this.cargarForm();
    this.usuarioService.getPerfil().subscribe(result => {
      console.log('result', result);
      if (result != null && result.Email === 'error_401') {
        this.perfilModel = new PerfilModel('', '', '', '', '', 0, null, 0, null, null, null, 1, null);
        this.getEstufas();
      } else {
        this.perfil = result as IPerfil;
        this.perfilModel = new PerfilModel(
          this.perfil.Email,
          this.perfil.DisplayName,
          this.perfil.telefono,
          this.perfil.nroKit,
          this.perfil.direccion,
          this.perfil.idEstufa,
          null,
          this.perfil.idModeloEstufa,
          null,
          this.perfil.nroCohabitantes,
          this.perfil.cantPromM3,
          this.perfil.cantPromMeses,
          this.perfil.cantPromMeses
        );

        this.perfilForm.controls.email.setValue(this.perfil.Email);
        this.perfilForm.controls.displayName.setValue(this.perfil.DisplayName);
        this.perfilForm.controls.telefono.setValue(this.perfil.telefono);
        this.perfilForm.controls.nroKit.setValue(this.perfil.nroKit);
        this.perfilForm.controls.direccion.setValue(this.perfil.direccion);
        this.perfilForm.controls.nroCohabitantes.setValue(this.perfil.nroCohabitantes);
        this.perfilForm.controls.cantPromM3.setValue(this.perfil.cantPromM3);
        this.perfilForm.controls.selectCantPromMeses.setValue(this.perfil.cantPromMeses);

        this.getEstufas();
      }
    },
      error => {
        console.log('error', error);
        this.getEstufas();
      }
    )
  }

  cargarForm() {
    this.buildPerfilForm();
  }

  public onSubmitPerfil(): void {
    this.loadingReg = true;



    let _idEstufa: number = 1;
    let _idModeloEstufa: number = 1;
    let _cantPromMeses: number = 1;

    if (this.perfilForm.value.selectMarcaEstufa != null)
      _idEstufa = this.perfilForm.value.selectMarcaEstufa;
    else
      _idEstufa = this.idEstufa;

    if (this.perfilForm.value.selectModeloEstufa != null)
      _idModeloEstufa = this.perfilForm.value.selectModeloEstufa;
    else
      _idModeloEstufa = this.idModeloEstufa;

    if (this.perfilForm.value.selectCantPromMeses != null)
      _cantPromMeses = this.perfilForm.value.selectCantPromMeses;
    else
      _cantPromMeses = this.cantPromMeses;

    let dataPost: IPerfil = {
      Id: this.perfilForm.value.Id,
      Email: this.perfilForm.value.email,
      HasRegistered: true,
      LoginProvider: '',
      DisplayName: this.perfilForm.value.displayName,
      Picture: '',
      telefono: this.perfilForm.value.telefono,
      nroKit: this.perfilForm.value.nroKit,
      direccion: this.perfilForm.value.direccion,
      idEstufa: _idEstufa,
      idModeloEstufa: _idModeloEstufa,
      nroCohabitantes: this.perfilForm.value.nroCohabitantes,
      cantPromM3: this.perfilForm.value.cantPromM3,
      cantPromMeses: _cantPromMeses
    }
    console.log('dataPost', dataPost);
    this.usuarioService.updatePerfil(dataPost)
      .subscribe(result => {
        this.actionError = true; // show the error

        if (result) {
          this.actionErrorMsg = 'Datos Guardados Correctamente';
          //this.msgs.push({ severity: 'success', summary: '', detail: 'Datos Guardados Correctamente' });
          this.navCtrl.setRoot(DashboardPage);
        }
        else {
          this.actionErrorMsg = 'Problemas al Guardar';
          //this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Problemas al Guardar' });
        }
        this.loadingReg = false;
      },
      error => {
        this.loadingReg = false;
      });
  }

  public buildPerfilForm(): void {
    this.perfilForm = this.fb.group({
      email: [this.perfilModel.email, [Validators.required, Validators.email]],
      displayName: [this.perfilModel.displayName, [Validators.required]],
      telefono: [this.perfilModel.telefono, [Validators.required]],
      nroKit: [this.perfilModel.nroKit, [Validators.required]],
      direccion: [this.perfilModel.direccion, [Validators.required]],
      selectMarcaEstufa: [this.perfilModel.selectMarcaEstufa, [Validators.required]],
      selectModeloEstufa: [this.perfilModel.selectModeloEstufa, [Validators.required]],
      nroCohabitantes: [this.perfilModel.nroCohabitantes, [Validators.required]],
      cantPromM3: [this.perfilModel.cantPromM3, [Validators.required]],
      selectCantPromMeses: [this.perfilModel.selectCantPromMeses, [Validators.required]]
    });

    // this.perfilForm.valueChanges.subscribe(data => this.onPerfilValueChanged(data));
  }

  public onPerfilValueChanged(data?: any) {
    // console.log('onPerfilValueChanged', data);
    if (!this.perfilForm) {
      return;
    }
    const form = this.perfilForm;
    // console.log('onPerfilValueChanged: form', form);
    for (const field in this.formPerfilErrors) {
      // clear previous error message (if any)
      //console.log('onPerfilValueChanged: field', field);
      this.formPerfilErrors[field] = '';
      const control = form.get(field);
      // console.log('onPerfilValueChanged: control', control);
      // console.log('onPerfilValueChanged: pristine', control.pristine);
      // console.log('onPerfilValueChanged: dirty', control.dirty);
      // console.log('onPerfilValueChanged: valid', control.valid);
      // if (control && control.dirty && !control.valid) {
      if (control && !control.valid) {
        //if (control && !control.valid) {
        const messages = this.validationMessages[field];
        console.log('onPerfilValueChanged: messages', messages);
        for (const key in control.errors) {
          console.log('onPerfilValueChanged: key', key);
          this.formPerfilErrors[field] += messages[key] + '<br />';
        }
      }
    }
  }

  public formPerfilErrors = {
    'email': '',
    'displayName': '',
    'telefono': '',
    'nroKit': '',
    'direccion': '',
    'nroCohabitantes': '',
    'cantPromM3': '',
    'selectMarcaEstufa': '',
    'selectModeloEstufa': '',
    'selectCantPromMeses': ''
  }

  private validationMessages = {
    'email': {
      'required': 'El Email es requerido',
      'email': 'El Email es inválido'
    },
    'displayName': {
      'required': 'El Nombre es requerido'
    },
    'telefono': {
      'required': 'El teléfono es requerido'
    },
    'nroKit': {
      'required': 'El nro de kit es requerido'
    },
    'direccion': {
      'required': 'La dirección es requerida'
    },
    'nroCohabitantes': {
      'required': 'El nro de cohabitantes es requerida'
    },
    'cantPromM3': {
      'required': 'La cantidad de M3 es requerida'
    },
    'selectMarcaEstufa': {
      'required': 'Debe seleccionar su estufa'
    },
    'selectModeloEstufa': {
      'required': 'Debe seleccionar el modelo de su estufa'
    },
    'selectCantPromMeses': {
      'required': 'El promedio en meses es requerido'
    }
  }

  goCompras() {
    this.navCtrl.setRoot(ComprasPage);
  }

  getEstufas(): void {
    this.listadosService.ObtenerMarcasEstufas().subscribe(result => {
      this.estufas = result as IEstufa[];
      this.setEstufa();
    });
  }

  getModelosEstufa(idEstufa: number): void {
    this.listadosService.ObtenerModelosEstufas(idEstufa).subscribe(result => {
      this.modelosEstufas = result as IModeloEstufa[];
      if (this.modelosEstufas.find(x => x.idModeloEstufa === this.perfil.idModeloEstufa))
        this.perfilForm.controls.selectModeloEstufa.setValue(this.modelosEstufas.find(x => x.idModeloEstufa === this.perfil.idModeloEstufa).idModeloEstufa);
      this.blokModel = false;
    });
  }

  setEstufa(): void {
    //if (this.pasosNacionalidades >= 2) {
    if (this.perfil && this.perfil.idEstufa) {
      this.perfilForm.controls.selectMarcaEstufa.setValue(this.estufas.find(x => x.idEstufa === this.perfil.idEstufa).idEstufa);
      // this.perfilForm.controls.nacionalidad.setValue(this.listNacionalidades.find(x => x.idNacionalidad === this.perfil.idNacionalidad));
      console.log('selectMarcaEstufa', this.perfilForm.value.selectMarcaEstufa);
      this.getModelosEstufa(this.perfil.idEstufa);
    }
  }

  cambiaMarca(marca) {
    this.blokModel = false;
    this.getModelosEstufa(marca);
  }

}
