import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { IGlosario } from '../../models/glosario';
import { GlosarioService } from '../../modules/services/glosario.service';

@Component({
  selector: 'page-glosario',
  templateUrl: 'glosario.html',
})
export class GlosarioPage {
  public glosario: IGlosario[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private glosarioService: GlosarioService) {
  }

  ionViewDidLoad() {
    console.log ('yes');
    this.getGlosario();
  }

  getGlosario(): void {
    this.glosarioService.getGlosario().subscribe(result => {
      this.glosario = result as IGlosario[];
    });
   }
}
