import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { NavController, NavParams, Nav, ViewController } from 'ionic-angular';
import { NavController, NavParams, Nav } from 'ionic-angular';

import { AuthService } from '../../modules/services/account/auth.service';
// import { ProfileService } from '../../modules/services/account/profile.service';
import { UsuarioService } from '../../modules/services/usuario.service';
import { DashboardPage } from '../dashboard/dashboard';

export class LoginModel {
    constructor(
        public user: string,
        public password: string,
    ) { }
}

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage implements OnInit {

    public showBtMenu: boolean = false;
    public showBtHome: boolean = false;

    @ViewChild(Nav) nav: Nav;
    public mostrarForm: boolean = false;
    public loading: boolean = false;
    public actionError: boolean;
    public actionErrorMsg: string;
    public returnUrl: string;
    public loginModel: LoginModel;
    public loginForm: FormGroup;

    public typePass: string = 'password';
    public showPass: boolean = false;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        // private viewCtrl: ViewController,
        private authService: AuthService,
        // private profileService: ProfileService,
        private usuarioService: UsuarioService,
        private fb: FormBuilder,
    ) {

    }

    showPassword() {
        this.showPass = !this.showPass;
        if (this.showPass) {
            this.typePass = 'text';
        } else {
            this.typePass = 'password';
        }
    }

    ionViewWillEnter() {
        // this.viewCtrl.showBackButton(false);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }

    ngOnInit(): void {
        this.cargarForm();
        this.usuarioService.getUsuarioConectado().subscribe(result => {
            console.log('result', result);
            if (result != null && result.Email === 'error_401') {
                this.mostrarForm = true;
            } else {
                this.navCtrl.setRoot(DashboardPage);
            }
        });

    }

    cargarForm() {
        this.loginModel = new LoginModel('', '');
        this.buildLoginForm();
    }

    public onSubmitLogin(): void {
        this.loading = true;
        this.authService.singIn(this.loginForm.value.user, this.loginForm.value.password)
            .subscribe(
            () => {
                console.log('onSubmitLogin userAutenticado');
                if (this.authService.isAuthenticated()) {
                    this.navCtrl.setRoot(DashboardPage);
                }
            },
            error => {
                this.actionError = true;
                this.actionErrorMsg = 'INVALIDLOGIN Economiza';
                this.loginForm.get('password').reset();

                this.loading = false;
            });
    }

    public buildLoginForm(): void {
        this.loginForm = this.fb.group({
            user: [this.loginModel.user, [
                Validators.required]
            ],
            password: [this.loginModel.password, Validators.required]
        });

        this.loginForm.valueChanges.subscribe(data => this.onLoginValueChanged(data));
    }

    public onLoginValueChanged(data?: any) {
        // console.log('onLoginValueChanged', data);
        if (!this.loginForm) {
            return;
        }
        const form = this.loginForm;
        // console.log('onLoginValueChanged: form', form);
        for (const field in this.formLoginErrors) {
            // clear previous error message (if any)
            //console.log('onLoginValueChanged: field', field);
            this.formLoginErrors[field] = '';
            const control = form.get(field);
            // console.log('onLoginValueChanged: control', control);
            // console.log('onLoginValueChanged: pristine', control.pristine);
            // console.log('onLoginValueChanged: dirty', control.dirty);
            // console.log('onLoginValueChanged: valid', control.valid);
            // if (control && control.dirty && !control.valid) {
            if (control && !control.valid) {
                //if (control && !control.valid) {
                const messages = this.validationMessages[field];
                // console.log('onLoginValueChanged: messages', messages);
                for (const key in control.errors) {
                    // console.log('onLoginValueChanged: key', key);
                    this.formLoginErrors[field] += messages[key] + '<br />';
                }
            }
        }
        // console.log("formLoginErrors", this.formLoginErrors);
    }

    public formLoginErrors = {
        'user': '',
        'password': ''
    }

    public validationMessages = {
        'user': {
            'required': 'El usuario es requerido'
        },
        'password': {
            'required': 'La clave es requerida'
        }
    }
}