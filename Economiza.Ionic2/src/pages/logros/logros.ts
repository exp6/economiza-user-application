import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// import { ILogros } from '../../models/logros';
// import { LogrosService } from '../../modules/services/logros.service';

import { IPerfil } from '../../models/Perfil';
import { UsuarioService } from '../../modules/services/usuario.service';
import { IotService } from '../../modules/services/iot.service';
import { ILogro } from '../../models/iot';

@Component({
  selector: 'page-logros',
  templateUrl: 'logros.html',
})
export class LogrosPage implements OnInit {

  public logros: ILogro[];
  public perfil: IPerfil;
  public showLogros: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    // private logrosService: LogrosService,
    public usuarioService: UsuarioService,
    public iotService: IotService
  ) {
  }

  ionViewDidLoad() {
    // this.getLogros();
    console.log('ionViewDidLoad LogrosPage');
  }

  ngOnInit(): void {
    this.usuarioService.getPerfil().subscribe(result => {
      // console.log('result', result);
      this.perfil = result as IPerfil;
      this.getLogros(this.perfil.nroKit);
    });
  }

  getLogros(channel: string): void {
    this.iotService.ObtenerLogrosUsuario(channel).subscribe(result => {
      //console.log('result', result);
      this.logros = result as ILogro[];
      if (result.length <= 0)
        this.showLogros = false;
    });
  }
}