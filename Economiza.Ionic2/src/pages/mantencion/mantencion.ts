import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { TutorialPage } from '../tutorial/tutorial';
import { UsuarioService } from '../../modules/services/usuario.service';
import { IotService } from '../../modules/services/iot.service';

import { IPerfil } from '../../models/Perfil';

@Component({
  selector: 'page-mantencion',
  templateUrl: 'mantencion.html',
})
export class MantencionPage {

  public tMantencion: string = "";
  public tTest: number = 28;
  public notificacionMantencion: string = "";

  public perfil: IPerfil;

  constructor(public navCtrl: NavController, public navParams: NavParams, public usuarioService: UsuarioService, public iotService: IotService) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MantencionPage');
  }
  doMantencion() {
    this.tMantencion = (7-this.tTest) + ' días';
  }
  goTutorial() {
    this.navCtrl.push(TutorialPage);
  }
  
  ngOnInit(): void {
    this.usuarioService.getPerfil().subscribe(result => {
      // console.log('result', result);
      this.perfil = result as IPerfil;
      // this.cargarDiasUso(this.perfil.nroKit);
      // this.cargarGraficos();

      this.iotService.ObtenerDiasFaltantesMantencion(this.perfil.nroKit).subscribe(result => {
        console.log('result', result);
        this.tTest = result as number;
        this.tMantencion = (7-this.tTest) + ' días';
        // this.notificacionMantencion = result as string;
      });

      this.iotService.ObtenerNotificacionMantencion(this.perfil.nroKit).subscribe(result => {
        // console.log('result', result);
        this.notificacionMantencion = result as string;
      });
    });
  }

  obtenerMantencion(channel) {
    this.iotService.ObtenerNotificacionMantencion(channel).subscribe(result => {
      // console.log('result', result);
      this.notificacionMantencion = result as string;
    });
  }

  realizarMantencion() {
    this.iotService.RealizarMantencion(this.perfil.nroKit).subscribe(result => {
      console.log('result', result);
      // this.notificacionMantencion = result as string;
    });

    this.iotService.ObtenerDiasFaltantesMantencion(this.perfil.nroKit).subscribe(result => {
      console.log('result', result);
      this.tTest = result as number;
      this.tMantencion = (7-this.tTest) + ' días';
      // this.notificacionMantencion = result as string;
    });

    this.iotService.ObtenerNotificacionMantencion(this.perfil.nroKit).subscribe(result => {
      // console.log('result', result);
      this.notificacionMantencion = result as string;
    });
  }
}
