import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { NavController, NavParams, Nav, ViewController } from 'ionic-angular';
import { NavController, NavParams, Nav } from 'ionic-angular';

import { PasswordMatchValidator } from '../../shared/password-match-validator';
import { AuthService } from '../../modules/services/account/auth.service';
import { AccountService } from '../../modules/services/account/account.service';
// import { ProfileService } from '../../modules/services/account/profile.service';
import { UsuarioService } from '../../modules/services/usuario.service';
import { DashboardPage } from '../dashboard/dashboard';

import { DatosPage } from '../datos/datos';
import { LoginPage } from '../login/login';

export class RegisterModel {
  constructor(
    public email: string,
    public displayName: string,
    public password: string,
    public confirmPassword: string
  ) { }
}

@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage implements OnInit {

  public showBtMenu: boolean = false;
  public showBtHome: boolean = false;

  @ViewChild(Nav) nav: Nav;
  public mostrarForm: boolean = false;
  public loadingReg: boolean = false;
  public actionError: boolean;
  public actionErrorMsg: string;
  public returnUrl: string;
  public registerModel: RegisterModel;
  public registerForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    // private viewCtrl: ViewController,
    private authService: AuthService,
    private accountService: AccountService,
    // private profileService: ProfileService,
    private usuarioService: UsuarioService,
    private fb: FormBuilder,
  ) {

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad RegistroPage');
  }

  goDatos() {
    this.navCtrl.setRoot(DatosPage);
  }

  goLogin() {
    this.navCtrl.push(LoginPage);
  }

  ngOnInit(): void {
    this.cargarForm();
    this.usuarioService.getUsuarioConectado().subscribe(result => {
      //console.log('result', result);
      if (result != null && result.Email === 'error_401') {
        this.mostrarForm = true;
      } else {
        this.navCtrl.setRoot(DashboardPage);
      }
    });

  }

  cargarForm() {
    this.registerModel = new RegisterModel('', '', '', '');
    this.buildRegisterForm();
  }

  public onSubmitRegister(): void {
    this.loadingReg = true;
    this.accountService.register(this.registerForm.value.email, this.registerForm.value.displayName, this.registerForm.value.password)
      .subscribe(
      () => {
        console.log('onSubmitRegister userAutenticado');
        if (this.authService.isAuthenticated()) {
          //this.navCtrl.setRoot(DatosPage);
          this.login();
        }
      },
      error => {
        console.log(error);
        this.actionError = true;
        this.actionErrorMsg = 'INVALIDRegister Economiza';
        this.registerForm.get('password').reset();

        this.loadingReg = false;
      });
  }

  public login(): void {
    this.authService.singIn(this.registerForm.value.email, this.registerForm.value.password)
      .subscribe(
      () => {
        console.log('onSubmitLogin userAutenticado');
        if (this.authService.isAuthenticated()) {
          this.navCtrl.setRoot(DatosPage);
        }
      },
      error => {
        console.log(error);
        this.actionError = true;
        this.actionErrorMsg = 'INVALIDRegister Economiza';
        this.registerForm.get('password').reset();

        this.loadingReg = false;
      });
  }

  public buildRegisterForm(): void {
    this.registerForm = this.fb.group({
      email: [this.registerModel.email, [
        Validators.required,
        Validators.email
      ]
      ],
      displayName: [this.registerModel.displayName, [
        Validators.required
      ]
      ],
      password: [this.registerModel.password, [
        Validators.required,
        Validators.minLength(6)
      ]
      ],
      confirmPassword: [this.registerModel.confirmPassword, Validators.required],
    }, { validator: PasswordMatchValidator('password', 'confirmPassword') });

    // this.registerForm.valueChanges.subscribe(data => this.onRegisterValueChanged(data));
  }

  public onRegisterValueChanged(data?: any) {
    // console.log('onRegisterValueChanged', data);
    if (!this.registerForm) {
      return;
    }
    const form = this.registerForm;
    // console.log('onRegisterValueChanged: form', form);
    for (const field in this.formRegisterErrors) {
      // clear previous error message (if any)
      //console.log('onRegisterValueChanged: field', field);
      this.formRegisterErrors[field] = '';
      const control = form.get(field);
      // console.log('onRegisterValueChanged: control', control);
      // console.log('onRegisterValueChanged: pristine', control.pristine);
      // console.log('onRegisterValueChanged: dirty', control.dirty);
      // console.log('onRegisterValueChanged: valid', control.valid);
      // if (control && control.dirty && !control.valid) {
      if (control && !control.valid) {
        //if (control && !control.valid) {
        const messages = this.validationMessages[field];
        console.log('onRegisterValueChanged: messages', messages);
        for (const key in control.errors) {
          console.log('onRegisterValueChanged: key', key);
          this.formRegisterErrors[field] += messages[key] + '<br />';
        }
      }
    }

    const confirmPasswordError = 'doesNotMatch';
    const confirmPasswordKey = 'confirmPassword';

    if (form.hasError(confirmPasswordError)) {
      const messages = this.validationMessages[confirmPasswordKey];
      this.formRegisterErrors[confirmPasswordKey] += messages[confirmPasswordError] + '<br />';
    }
    console.log("formRegisterErrors", this.formRegisterErrors);
  }

  public formRegisterErrors = {
    'email': '',
    'displayName': '',
    'password': '',
    'confirmPassword': ''
  }

  private validationMessages = {
    'email': {
      'required': 'El Email es requerido',
      'email': 'El Email es inválido'
    },
    'displayName': {
      'required': 'El Nombre es requerido'
    },
    'password': {
      'required': 'La clave es requerida',
      'minlength': 'La clave debe tener al menos 6 caracteres de largo'
    },
    'confirmPassword': {
      'required': 'La confirmación de la clave es requerida',
      'doesNotMatch': 'La contraseña y la contraseña de confirmación no coinciden'
    }
  }
}