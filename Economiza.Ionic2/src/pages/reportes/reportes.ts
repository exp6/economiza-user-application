import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Chart } from 'chart.js';

import { UsuarioService } from '../../modules/services/usuario.service';
import { IotService } from '../../modules/services/iot.service';
import { IHistoriaUso } from '../../models/iot';

@Component({
  selector: 'page-reportes',
  templateUrl: 'reportes.html',
})
export class ReportesPage implements AfterViewInit {
  @ViewChild('lineCanvasO') lineCanvasO;
  @ViewChild('lineCanvasT') lineCanvasT;
  @ViewChild('lineCanvasCO') lineCanvasCO;
  @ViewChild('lineCanvasMP') lineCanvasMP;
  lineChartO: any;
  lineChartT: any;
  lineChartCO: any;
  lineChartMP: any;

  public historia: IHistoriaUso[];
  showGrafico: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public iotService: IotService,
    public usuarioService: UsuarioService
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportesPage');
  }

  ngAfterViewInit() {
    this.usuarioService.getPerfil().subscribe(result => {
      this.cargarDatosGraficos(result.nroKit);
    });
  }

  cargarDatosGraficos(channel) {
    this.iotService.ObtenerHistoria30Dias(channel).subscribe(result => {
      // console.log('result', result);
      this.historia = result as IHistoriaUso[];
      this.cargarGraficos();
    });
  }

  optionChart: any = {
    legend: {
      display: false,
      labels: { boxWidth: 10, fontStyle: '300', fontFamily: "'Roboto Condensed', sans-serif", fontSize: 13 },
      position: 'bottom'
    },
    elements: {
      line: { tension: 0, fill: false, borderWidth: 2 },
      point: { radius: 2, hitRadius: 10, hoverRadius: 5 },
      rectangle: { backgroundColor: "rgba(255,255,255,1)", borderWidth: 10, borderColor: '#ffffff' }
    },
    tooltips: { enabled: false },
    spanGaps: false,
    scales: {
      yAxes: [{
        gridLines: { display: false, color: '#ffffff' }
      }],
      xAxes: [{
        gridLines: { display: false, color: '#ffffff' }
      }]
    },

  };

  cargarGraficos() {
    this.showGrafico = true;
    let labelHoras = new Array();
    let dataO = new Array();
    let dataT = new Array();
    let dataCO = new Array();
    let dataMP = new Array();

    this.historia.forEach((item, index) => {
      labelHoras.push(item.fechaFormat);
      dataO.push(item.PROM_O2);
      dataT.push(item.PROM_TEMP);
      dataCO.push(item.PROM_CO);
      dataMP.push(item.PROM_MP);
    });

    console.log(labelHoras);
    console.log(dataO);

    this.lineChartO = new Chart(this.lineCanvasO.nativeElement, {
      type: 'line',
      data: {
        labels: labelHoras,
        datasets: [
          {
            label: "Oxígeno (O)",
            backgroundColor: "rgba(0,136,201, 1)",
            borderColor: "rgba(0,136,201, 1)",
            pointBorderColor: "rgba(0,136,201, 1)",
            pointBackgroundColor: "rgba(0,136,201, 1)",
            pointHoverBackgroundColor: "rgba(255,255,255,1)",
            pointHoverBorderColor: "rgba(255,255,255,1)",
            data: dataO
          }
        ]
      },// dataO,
      options: this.optionChart
    });

    this.lineChartT = new Chart(this.lineCanvasT.nativeElement, {
      type: 'line',
      data: {
        labels: labelHoras,
        datasets: [
          {
            label: "Oxígeno (O)",
            backgroundColor: "rgba(0,136,201, 1)",
            borderColor: "rgba(0,136,201, 1)",
            pointBorderColor: "rgba(0,136,201, 1)",
            pointBackgroundColor: "rgba(0,136,201, 1)",
            pointHoverBackgroundColor: "rgba(255,255,255,1)",
            pointHoverBorderColor: "rgba(255,255,255,1)",
            data: dataT
          }
        ]
      },// dataO,
      options: this.optionChart
    });

    this.lineChartCO = new Chart(this.lineCanvasCO.nativeElement, {
      type: 'line',
      data: {
        labels: labelHoras,
        datasets: [
          {
            label: "Oxígeno (O)",
            backgroundColor: "rgba(0,136,201, 1)",
            borderColor: "rgba(0,136,201, 1)",
            pointBorderColor: "rgba(0,136,201, 1)",
            pointBackgroundColor: "rgba(0,136,201, 1)",
            pointHoverBackgroundColor: "rgba(255,255,255,1)",
            pointHoverBorderColor: "rgba(255,255,255,1)",
            data: dataCO
          }
        ]
      },// dataO,
      options: this.optionChart
    });

    this.lineChartMP = new Chart(this.lineCanvasMP.nativeElement, {
      type: 'line',
      data: {
        labels: labelHoras,
        datasets: [
          {
            label: "Oxígeno (O)",
            backgroundColor: "rgba(0,136,201, 1)",
            borderColor: "rgba(0,136,201, 1)",
            pointBorderColor: "rgba(0,136,201, 1)",
            pointBackgroundColor: "rgba(0,136,201, 1)",
            pointHoverBackgroundColor: "rgba(255,255,255,1)",
            pointHoverBorderColor: "rgba(255,255,255,1)",
            data: dataMP
          }
        ]
      },// dataO,
      options: this.optionChart
    });

    // this.lineChartT = new Chart(this.lineCanvasT.nativeElement, {
    //   type: 'line',
    //   data: dataT,
    //   options: this.optionChart
    // });

    // this.lineChartCO = new Chart(this.lineCanvasCO.nativeElement, {
    //   type: 'line',
    //   data: dataCO,
    //   options: this.optionChart
    // });

    // this.lineChartMP = new Chart(this.lineCanvasMP.nativeElement, {
    //   type: 'line',
    //   data: dataMP,
    //   options: this.optionChart
    // });
  }
}