import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, AlertController } from 'ionic-angular';

import { DashboardPage } from '../dashboard/dashboard';

import { ITips } from '../../models/tips';
import { TipsService } from '../../modules/services/tips.service';

@Component({
  selector: 'page-tips',
  templateUrl: 'tips.html',
})
export class TipsPage {

  cantidadTips: number;
  public tips: ITips[];

  public showBtMenu: boolean = false;
  public showBtHome: boolean = false;
  public showBtNoMore: boolean = false;

  @ViewChild(Slides) slides: Slides;

  constructor(
    public alerCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private tipsService: TipsService) {
      this.cantidadTips = navParams.get('cantTips');
      this.showBtMenu = navParams.get('fromMenu');
  }

  ionViewDidLoad() {
    this.getTips();
    console.log('ionViewDidLoad TipsPage');
  }

  getTips(): void {
    this.tipsService.getTips(this.cantidadTips).subscribe(result => {
      this.tips = result as ITips[];
    });
   }

  tipCambia() {
    this.showBtNoMore = false;
  }
  tipCambio() {
    if(this.cantidadTips) { 
      if (this.slides.isEnd()){
        this.showBtNoMore = true;
      } else {
        this.showBtNoMore = false;
      }
    }
  }
  noMostrarMas() {
    let alert = this.alerCtrl.create({
      message: 'Los tips no se volverán a mostrar al iniciar la aplicación, pero podrá revisarlos cada vez que lo necesite a través del menú tips de la aplicación.',
      buttons: [
        {
          text: 'Descartar',
          handler: () => { }
        },
        {
          text: 'Aceptar',
          handler: () => { this.navCtrl.setRoot(DashboardPage); }
        }
      ]
    });
    alert.present()
  }
  
  goHome(){
    this.navCtrl.setRoot(DashboardPage);
  }
}