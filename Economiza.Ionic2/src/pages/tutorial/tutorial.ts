import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {
  slides = [
    {
      title: "¿Cómo debo encender mi filtro?",
      subtitle: "Paso 01",
      description: "Antes de encender la estufa, tire la vara del filtro hacia atrás, para situarlo en la posición B indicada.",
      image: "assets/img/instruccion_uso_01.png",
    },
    {
      title: "¿Cómo debo encender mi filtro?",
      subtitle: "Paso 02",
      description: "Transcurridos 15 a 20 minutos de haber encendido el fuego, empuje la varilla del filtro hacia adentro, tal como lo indica la posición A. De esta forma, su filtro estará en funcionamiento",
      image: "assets/img/instruccion_uso_02.png",
    },
    {
      title: "¿Cómo debo mantener mi filtro?",
      subtitle: "Paso 01",
      description: "Retire los tornillos que se encuentran en la posición indicada en la foto, retirando la tapa del filtro.",
      image: "assets/img/instruccion_uso_03.png",
    },
    {
      title: "¿Cómo debo mantener mi filtro?",
      subtitle: "Paso 02",
      description: "Al retirar la tapa, podrá a la vez retirar el cerámico interior. Para finalizar la mantención, pase una brocha o escobilla sobre el cerámico, o bien utilice una aspiradora para eliminar cualquier residuo.<br /><br />Queda prohibido utilizar brochas de metal, o lijas, o introducir cualquier tipo de elemento metálico en los orificios del cerámico.",
      image: "assets/img/instruccion_uso_04.png",
    },
    {
      title: "¿Cómo debo mantener mi filtro?",
      subtitle: "Paso 03",
      description: "Finalmente vuelva a introducir el cerámico y vuelva a apretar los tornillos de la tapa.<br /><br />¡Ya está en condiciones de volver a utilizar su filtro!",
      image: "assets/img/instruccion_uso_04.png",
    },
    {
      title: "¿Cómo debo limpiar mi cañón?",
      subtitle: "",
      description: "En caso de querer limpiar el cañon de su estufa, ponga el filtro en la posición B antes indicada.<br /><br />RECUERDE SIEMPRE TENER EL FILTRO EN POSICIÓN B ANTES DE ENCENDER EL FUEGO.<br /><br /><center>¡A descontaminar se ha dicho!</center>",
      image: "assets/img/instruccion_uso_01.png",
    }
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorialPage');
  }

}
