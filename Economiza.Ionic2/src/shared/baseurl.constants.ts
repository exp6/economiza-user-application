import { InjectionToken } from '@angular/core';

export const ORIGIN_URL = new InjectionToken<string>('ORIGIN_URL');

export const SERVICIO_API: string = "http://186.103.210.138/Economiza/Servicios";
// export const SERVICIO_API: string = "http://localhost/ServiciosEconomiza";
export const SERVICIO_API_CLOUD: string = "http://economiza-service.azurewebsites.net"