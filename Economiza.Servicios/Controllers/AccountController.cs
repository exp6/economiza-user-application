﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Economiza.Servicios.Models;
using Economiza.Servicios.Providers;
using Economiza.Servicios.Results;
using Economiza.Business;
using Economiza.Entities;
using Economiza.Utilities;

namespace Economiza.Servicios.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public async Task<UserInfoFullViewModel> GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            //return new UserInfoViewModel
            //{
            //    Email = User.Identity.GetUserName(),
            //    HasRegistered = externalLogin == null,
            //    LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            //};
            if (externalLogin != null)
            {

                UtilesLog.LogInfo(externalLogin.LoginProvider, "GetUserInfoExternal");
                var datosFull = await DatosSociales.ObtenerDatosUsuario(externalLogin.LoginProvider, externalLogin.ProviderKey);
                //https://graph.facebook.com/10210580702295466?fields=id,name,picture,birthday,email,gender&access_token=1235519283133142|01244f563e89b8d15b7104dd40b3936f
                var _user = AdministracionSsoMngr.GetInstance().ObtenerUsuarioByEmail(datosFull.Email);
                return new UserInfoFullViewModel
                {
                    Email = datosFull.Email, //User.Identity.GetUserName(),
                    EmailConfirmed = _user.EmailConfirmed,
                    DisplayName = datosFull.DisplayName,
                    Picture = datosFull.Picture,
                    HasRegistered = externalLogin == null,
                    LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
                };
            }
            else
            {
                var _user = AdministracionSsoMngr.GetInstance().ObtenerUsuarioByEmail(User.Identity.GetUserName());
                return new UserInfoFullViewModel
                {
                    Email = _user.Email, //User.Identity.GetUserName(),
                    EmailConfirmed = _user.EmailConfirmed,
                    DisplayName = _user.DisplayName,
                    Picture = _user.Picture,
                    HasRegistered = externalLogin == null,
                    LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
                };
            }
        }

        // GET api/Account/UserInfoExternal
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfoExternal")]
        public async Task<UserInfoFullViewModel> GetUserInfoExternal()
        {
            string direccion = string.Empty;
            string telefono = string.Empty;
            long nroKit = 0;
            int idEstufa = 0;
            int idModeloEstufa = 0;
            int nroCohabitantes = 0;
            int cantPromM3 = 0;
            int cantPromMeses = 0;
            bool EmailConfirmed = false;
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);
            if (externalLogin != null && !string.IsNullOrEmpty(externalLogin.LoginProvider))
            {
                var _HasRegistered = false;
                Utilities.UtilesLog.LogInfo(externalLogin.LoginProvider, "GetUserInfoExternal");
                var datosFull = await DatosSociales.ObtenerDatosUsuario(externalLogin.LoginProvider, externalLogin.ProviderKey);

                var datosFull2 = AdministracionSsoMngr.GetInstance().ObtenerUsuarioByEmail(datosFull.Email);
                if (datosFull2 != null && datosFull.Id != null)
                {
                    _HasRegistered = true;
                    direccion = datosFull2.direccion;
                    telefono = datosFull2.telefono;
                    nroKit = datosFull2.nroKit;
                    idEstufa = datosFull2.idEstufa;
                    idModeloEstufa = datosFull2.idModeloEstufa;
                    nroCohabitantes = datosFull2.nroCohabitantes;
                    cantPromM3 = datosFull2.cantPromM3;
                    cantPromMeses = datosFull2.cantPromMeses;
                    EmailConfirmed = datosFull2.EmailConfirmed;
                }
                else
                    _HasRegistered = AdministracionSsoMngr.GetInstance().ObtenerUsuarioLogin(datosFull.Email).Any(u => u.LoginProvider == externalLogin.LoginProvider && u.ProviderKey == externalLogin.ProviderKey);

                //db.AspNetUserLogins.Any(u => u.LoginProvider == externalLogin.LoginProvider && u.ProviderKey == externalLogin.ProviderKey);

                //var dirChile = PerfilMngr.GetInstance().ObtenerDireccion(datosFull.Id, TipoDireccion.Chile);
                //var dirOrigen = PerfilMngr.GetInstance().ObtenerDireccion(datosFull.Id, TipoDireccion.Origen);

                //https://graph.facebook.com/10210580702295466?fields=id,name,picture,birthday,email,gender&access_token=1235519283133142|01244f563e89b8d15b7104dd40b3936f
                return new UserInfoFullViewModel
                {
                    Id = datosFull.Id,
                    Email = datosFull.Email, //User.Identity.GetUserName(),
                    EmailConfirmed = EmailConfirmed,
                    DisplayName = datosFull.DisplayName,
                    Picture = datosFull.Picture,
                    direccion = direccion,
                    telefono = telefono,
                    nroKit = nroKit,
                    idEstufa = idEstufa,
                    idModeloEstufa = idModeloEstufa,
                    nroCohabitantes = nroCohabitantes,
                    cantPromM3 = cantPromM3,
                    cantPromMeses = cantPromMeses,
                    HasRegistered = _HasRegistered,
                    LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null,
                    withSocial = true
                };

            }
            else
            {
                //var datosFull = db.AspNetUsers.Where(u => u.Email == User.Identity.Name).FirstOrDefault();
                var datosFull = AdministracionSsoMngr.GetInstance().ObtenerUsuarioByEmail(User.Identity.Name);
                var _externalLogin = AdministracionSsoMngr.GetInstance().ObtenerUsuarioLogin(datosFull.Email).FirstOrDefault(); // db.AspNetUserLogins.Where(u => u.UserId == datosFull.Id).FirstOrDefault();

                var _imgFb = "";
                if (_externalLogin != null)
                {
                    var datosFb = await DatosSociales.ObtenerDatosUsuario(_externalLogin.LoginProvider, _externalLogin.ProviderKey);
                    _imgFb = datosFb.Picture;
                    if (!string.IsNullOrEmpty(_imgFb))
                    {
                        AdministracionSsoMngr.GetInstance().ActualizarFotoPerfil(datosFull.Email, _imgFb);
                    }
                }

                //var dirChile = PerfilMngr.GetInstance().ObtenerDireccion(datosFull.Id, TipoDireccion.Chile);
                //var dirOrigen = PerfilMngr.GetInstance().ObtenerDireccion(datosFull.Id, TipoDireccion.Origen);


                return new UserInfoFullViewModel
                {
                    Id = datosFull.Id,
                    Email = datosFull.Email,
                    EmailConfirmed = datosFull.EmailConfirmed,
                    DisplayName = datosFull.DisplayName,
                    Picture = datosFull.Picture,
                    HasRegistered = true,

                    direccion = datosFull.direccion,
                    telefono = datosFull.telefono,
                    nroKit = datosFull.nroKit,
                    idEstufa = datosFull.idEstufa,
                    idModeloEstufa = datosFull.idModeloEstufa,
                    nroCohabitantes = datosFull.nroCohabitantes,
                    cantPromM3 = datosFull.cantPromM3,
                    cantPromMeses = datosFull.cantPromMeses,
                    withSocial = false,

                    LoginProvider = _externalLogin != null ? _externalLogin.LoginProvider : null
                };
            }
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("Error de inicio de sesión externo.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("El inicio de sesión externo ya está asociado a una cuenta.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email //, DisplayName = model.DisplayName
            };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            else
            {
                var modelPerfil = new UsuarioTO();
                modelPerfil.UserName = model.Email;
                modelPerfil.DisplayName = model.DisplayName;
                AdministracionSsoMngr.GetInstance().ActualizarPerfil(modelPerfil);
            }

            return Ok();
        }

        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Aplicaciones auxiliares

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No hay disponibles errores ModelState para enviar, por lo que simplemente devuelva un BadRequest vacío.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        public class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits debe ser uniformemente divisible por 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        private static class DatosSociales
        {
            #region Metodos Nuevos
            public static async Task<UsuarioTO> ObtenerDatosUsuario(string provider, string userId)
            {
                var user = new UsuarioTO();

                HttpClient client = new HttpClient();

                if (provider == "Facebook")
                {
                    var _urlProfile = string.Format("https://graph.facebook.com/{0}?fields=id,name,email,gender,picture.width(10000).height(10000)&access_token={1}", userId, Startup.facebookAppToken);
                    UtilesLog.LogInfo(_urlProfile, "DatosSociales");
                    Uri uri = new Uri(_urlProfile);
                    HttpResponseMessage response = await client.GetAsync(uri);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        FacebookDataTO data = Newtonsoft.Json.JsonConvert.DeserializeObject<FacebookDataTO>(content);
                        user.Id = data.id;
                        user.DisplayName = data.name;
                        user.Email = (data.email != null) ? data.email : "";
                        user.Picture = (data.picture != null) ? data.picture.data.url : "";
                    }
                }
                else if (provider == "Google")
                {
                    // not implemented yet
                    return null;
                    //verifyTokenEndPoint = string.Format("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}", accessToken);
                }
                else if (provider == "Twitter")
                {
                    //verifyTokenEndPoint = string.Format("https://api.twitter.com/1.1/account/verify_credentials.json");
                    // don't need verifyAppEndPoint as our twitter app automatically gets verified. The token will be invalid if it was issues by some other spoofing app.
                    //add authorization headers here...
                    //client.DefaultRequestHeaders.Add("Authorization", string.Format("your authorizations here&access_Token={0}&other stuff", accessToken));
                    return null; // remove return on implementation.
                }
                else
                {
                    return null;
                }

                return user;
            }
            #endregion
        }

        #endregion
    }
}
