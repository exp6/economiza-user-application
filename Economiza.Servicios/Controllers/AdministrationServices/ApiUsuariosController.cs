﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Economiza.Business;
using Economiza.Entities;
using Economiza.Servicios.Models;

namespace Economiza.Servicios.Controllers.AdministrationServices
{
    [Authorize]
    [RoutePrefix("ApiUsuarios")]
    public class ApiUsuariosController : ApiBaseController
    {
        [Route("ObtenerUsuario")]
        [HttpGet]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        public UsuarioTO ObtenerUsuario()
        {
            try
            {
                var user = UsuarioConectado();//Obtener usuario conectado 
                logger.Info("Usuario conectado es: " + user.UserName);

                var _user = AdministracionSsoMngr.GetInstance().ObtenerUsuarioByEmail(user.UserName);
                return _user;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return null;
            }
        }

        [Route("UpdatePerfil")]
        [HttpPost]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        public bool UpdatePerfil(UserInfoFullViewTO model)
        {
            try
            {
                //var user = UsuarioConectado();//Obtener usuario conectado
                //logger.Info("Usuario conectado es: " + user.UserName);
                logger.Info("UpdatePerfil()");
                //logger.Info("Usuario perfil latitud: " + model.direccionChile.coordenadas.Latitud);
                var ret = PerfilMngr.GetInstance().ActualizarPerfil(model);
                return ret;
            }
            catch (Exception ex)
            {
                logger.Info(ex.ToString());
                LogException(ex);
                return false;
            }
        }
    }
}
