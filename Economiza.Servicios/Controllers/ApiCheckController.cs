﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Economiza.Servicios.Controllers
{
    [RoutePrefix("ApiCheck")]
    public class ApiCheckController : ApiBaseController
    {
        [Route("Internet")]
        [HttpGet]
        public bool Internet()
        {
            try
            {
                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }
        }
    }
}
