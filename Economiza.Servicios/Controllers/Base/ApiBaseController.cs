﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Economiza.Entities;
using Economiza.Servicios.Helper;
using NLog;

namespace Economiza.Servicios.Controllers
{
    public class ApiBaseController : ApiController
    {
        protected Logger logger { get; private set; }

        protected ApiBaseController()
        {
            logger = LogManager.GetLogger(GetType().FullName);
        }

        public UsuarioTO UsuarioConectado()
        {
            UtilesUsuario utiluser = new Helper.UtilesUsuario();
            return utiluser.ObtenerUsuario();
        }

        public void LogException(Exception e)
        {
            logger.Error(string.Format("Excepcion capturada: {0} Origen: {1} Mensaje: {2} StackTrace: {3}", e.GetType(), e.Source, e.Message, e.StackTrace));
        }
    }
}
