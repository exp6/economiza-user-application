﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Economiza.Business;
using Economiza.Entities;
using Economiza.Servicios.Models;

namespace Economiza.Servicios.Controllers.IotServices
{
    [Authorize]
    [RoutePrefix("ApiIot")]
    public class ApiIotController : ApiBaseController
    {
        [Route("ObtenerHistoria30Dias/{channel}")]
        [HttpGet]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        public IQueryable<HistoriaUsoTO> ObtenerHistoria30Dias(string channel)
        {
            try
            {
                var user = UsuarioConectado();//Obtener usuario conectado 
                logger.Info("Usuario conectado es: " + user.UserName);

                var _historia = IOTMngr.GetInstance().ObtenerHistoria30Dias(channel);
                return _historia;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return null;
            }
        }

        [Route("ObtenerLogrosUsuario/{channel}")]
        [HttpGet]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        public IQueryable<LogrosTO> ObtenerLogrosUsuario(string channel)
        {
            try
            {
                var user = UsuarioConectado();//Obtener usuario conectado 
                logger.Info("Usuario conectado es: " + user.UserName);

                var _logros = IOTMngr.GetInstance().ObtenerLogrosUsuario(channel);
                return _logros;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return null;
            }
        }

        [Route("ObtenerDiasUsoFiltro/{channel}")]
        [HttpGet]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        public int ObtenerDiasUsoFiltro(string channel)
        {
            try
            {
                var user = UsuarioConectado();//Obtener usuario conectado 
                logger.Info("Usuario conectado es: " + user.UserName);

                var _dias = IOTMngr.GetInstance().ObtenerDiasUsoFiltro(channel);
                return _dias;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return 0;
            }
        }
    }
}
