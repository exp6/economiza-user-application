﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Economiza.Business;
using Economiza.Entities;
using Economiza.Servicios.Models;
using System.Configuration;
using System.Text;

namespace Economiza.Servicios.Controllers.OperacionServices
{
    [AllowAnonymous]
    [RoutePrefix("ApiEmail")]
    public class ApiEmailController : ApiBaseController
    {
        //private ApplicationUserManager _userManager;

        //public ApplicationUserManager UserManager
        //{
        //    get
        //    {
        //        return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        //    }
        //    private set
        //    {
        //        _userManager = value;
        //    }
        //}

        [Route("SendMensajeContacto")]
        [HttpPost]
        [Authorize]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        public async Task<bool> SendMensajeContacto(EmailTO model)
        {
            try
            {
                var destination = "";
                try
                {
                    destination = ConfigurationManager.AppSettings["EmailContacto"];
                }
                catch
                {
                    destination = "enrique.hernandez@basedos.cl";
                }

                var user = UsuarioConectado();//Obtener usuario conectado 
                var _user = AdministracionSsoMngr.GetInstance().ObtenerUsuarioByEmail(user.UserName);

                var sb = new StringBuilder();
                sb.Append("Datos Cliente: <br /><br />");
                sb.Append(string.Format("<strong>Id</strong>: {0}<br /> ", user.Id));
                sb.Append(string.Format("<strong>Nombre</strong>: {0}<br /> ", _user.DisplayName));
                sb.Append(string.Format("<strong>Email</strong>: {0}<br /> ", user.Email));
                sb.Append(string.Format("<br /><br /><strong>Mensaje</strong>: <br /> {0} ", model.mensaje));

                //await UserManager.SendEmailAsync(null, titulo, mensaje);
                var message = new IdentityMessage();
                message.Body = sb.ToString();
                message.Destination = destination;
                message.Subject = string.Format("Mensaje Contacto: {0}", model.asunto);
                await Helper.Email.SendEmail.SendAsync(message);
                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //if (_userManager != null)
                //{
                //    _userManager.Dispose();
                //    _userManager = null;
                //}
            }
            base.Dispose(disposing);
        }
    }
}
