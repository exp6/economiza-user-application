﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Economiza.Business;
using Economiza.Entities;
using Economiza.Servicios.Models;

namespace Economiza.Servicios.Controllers.OperacionServices
{
    [AllowAnonymous]
    [RoutePrefix("ApiListados")]
    public class ApiListadosController : ApiBaseController
    {
        [Route("ObtenerMarcasEstufas")]
        [HttpGet]
        [AllowAnonymous]
        public IQueryable<EstufasTO> ObtenerMarcasEstufas()
        {
            try
            {
                //var user = UsuarioConectado();//Obtener usuario conectado 
                //logger.Info("Usuario conectado es: " + user.UserName);

                return ListadoMngr.GetInstance().ObtenerMarcasEstufas();
            }
            catch (Exception ex)
            {
                LogException(ex);
                return null;
            }
        }

        [Route("ObtenerModelosEstufas/{idEstufa}")]
        [HttpGet]
        [AllowAnonymous]
        public IQueryable<ModelosEstufasTO> ObtenerModelosEstufas(int idEstufa)
        {
            try
            {
                //var user = UsuarioConectado();//Obtener usuario conectado 
                //logger.Info("Usuario conectado es: " + user.UserName);

                return ListadoMngr.GetInstance().ObtenerModelosEstufas(idEstufa);
            }
            catch (Exception ex)
            {
                LogException(ex);
                return null;
            }
        }

        [Route("ObtenerTips/{cantidad}")]
        [HttpGet]
        [AllowAnonymous]
        public IQueryable<TipsTO> ObtenerTips(int cantidad)
        {
            try
            {
                //var user = UsuarioConectado();//Obtener usuario conectado 
                //logger.Info("Usuario conectado es: " + user.UserName);
                var tips = ListadoMngr.GetInstance().ObtenerTips();
                if (cantidad > 0)
                    tips = tips.Take(cantidad);

                return tips;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return null;
            }
        }
    }
}
