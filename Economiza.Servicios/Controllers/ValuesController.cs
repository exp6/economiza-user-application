﻿using Economiza.Servicios.Helper.TelemetryManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Economiza.Servicios.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public IHttpActionResult Post([FromBody]string value)
        {
            string deviceMsgStr = value;
            dynamic msg = JsonConvert.DeserializeObject(deviceMsgStr);
            Dictionary<string, string> deviceMsg = new Dictionary<string, string>();
            string deviceId = msg["DeviceId"];
            string oxygen = msg["Oxygen"];
            string temperature = msg["Temperature"];
            deviceMsg.Add("deviceId", deviceId);
            deviceMsg.Add("oxygen", oxygen);
            deviceMsg.Add("temperature", temperature);
            
            System.Threading.ThreadPool.QueueUserWorkItem(a => DeviceIoTTelemetryService.Run(deviceMsg).Wait());
            
            return Ok();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
