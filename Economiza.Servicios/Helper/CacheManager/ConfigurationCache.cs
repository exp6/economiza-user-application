﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economiza.Servicios.Helper.CacheManager
{
    public enum BackendCacheKey
    {
        KeyUsuariosSMSGO
    }

    public static class ConfigurationCache
    {
        private static Dictionary<string, string> llaves = new Dictionary<string, string>();
        static ConfigurationCache()
        {
            llaves.Add("KeyUsuariosSMSGO", @"MemoryCacheUsuariosSMSGO_Agregador_{0}");
        }

        public static string Mensaje(BackendCacheKey backendMessage)
        {
            return llaves[backendMessage.ToString()];
        }
    }

}
