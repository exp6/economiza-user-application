﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;
using System.Web;

namespace Economiza.Servicios.Helper.CacheManager
{
    public static class UtilesCache
    {
        public static void SetCache(string key, Object obj)
        {
            double MINUTOSCACHE;
            try
            {
                MINUTOSCACHE = Convert.ToDouble(ConfigurationManager.AppSettings["MINUTOSCACHE"]);
                if (MINUTOSCACHE < 1)
                {
                    MINUTOSCACHE = 5;
                }
            }
            catch
            {
                MINUTOSCACHE = 5;
            }
            SetCache(key, obj, MINUTOSCACHE);
        }

        public static void SetCache(string key, Object obj, double minutosCache)
        {
            string strCacheElementKey = key.ToUpper();
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(strCacheElementKey))
            {
                cache.Remove(strCacheElementKey);
            }
            var policy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(minutosCache),
                //SlidingExpiration = TimeSpan.FromMinutes(minutosCache), //System.Web.Caching.Cache.NoSlidingExpiration,
                Priority = CacheItemPriority.Default
            };
            CacheItem itemCache = new CacheItem(strCacheElementKey, obj);
            cache.Set(itemCache, policy);
        }

        #region GetTO

        /// <summary>
        /// Gets the dataset from the cache related to the command
        /// </summary>
        public static List<T> GetCache<T>(string key)
        {
            string strCacheElementKey = key.ToUpper();
            ObjectCache cache = MemoryCache.Default;
            if (cache.Contains(strCacheElementKey))
            {
                var listado = (List<T>)cache.Get(strCacheElementKey);
                if (listado.Count <= 0)
                {
                    cache.Remove(strCacheElementKey);
                    return null;
                }
                else
                    return listado;
            }
            else
            {
                return null;
            }
        }
        #endregion

        /// <summary>
        /// Clears the cache for a table (strTableName)
        /// </summary>
        public static void ClearCache(string key)
        {
            string strCacheElementKey = key.ToUpper();
            ObjectCache cache = MemoryCache.Default;
            if (cache.Contains(strCacheElementKey))
                cache.Remove(strCacheElementKey);
        }

        public static void CleanAllCache()
        {
            Utilities.UtilesLog.LogInfo("Iniciando Limpieza Cache", "cl.basedos.digevo.servicios.Helper.CacheManager.UtilesCache.CleanAllCache");
            List<string> cacheKeys = MemoryCache.Default.Select(kvp => kvp.Key).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                Utilities.UtilesLog.LogInfo("Limpiando Cache Key: " + cacheKey, "cl.basedos.digevo.servicios.Helper.CacheManager.UtilesCache.CleanAllCache");
                MemoryCache.Default.Remove(cacheKey);
            }
            Utilities.UtilesLog.LogInfo("Finalizando Limpieza Cache", "cl.basedos.digevo.servicios.Helper.CacheManager.UtilesCache.CleanAllCache");
        }

        public static void CleanCampañasCache()
        {
            Utilities.UtilesLog.LogInfo("Iniciando Limpieza Cache", "cl.basedos.digevo.servicios.Helper.CacheManager.UtilesCache.CleanCampañasCache");
            List<string> cacheKeys = MemoryCache.Default.Select(kvp => kvp.Key).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                if (cacheKey.StartsWith("MEMORYCACHECAMPANAS"))
                {
                    Utilities.UtilesLog.LogInfo("Limpiando Cache Key: " + cacheKey, "cl.basedos.digevo.servicios.Helper.CacheManager.UtilesCache.CleanCampañasCache");
                    MemoryCache.Default.Remove(cacheKey);
                }
            }
            Utilities.UtilesLog.LogInfo("Finalizando Limpieza Cache", "cl.basedos.digevo.servicios.Helper.CacheManager.UtilesCache.CleanCampañasCache");
        }

        /// <summary>
        /// Clonar listado T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static IList<T> CloneList<T>(IList<T> source) where T : ICloneable
        {
            IList<T> clone = new List<T>(source.Count);
            foreach (T t in source)
            {
                clone.Add((T)t.Clone());
            }
            return clone;
        }
    }
}