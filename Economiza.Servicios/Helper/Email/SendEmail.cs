﻿using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Economiza.Servicios.Helper.Email
{
    public class SendEmail
    {
        public static Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            sendMail(message);
            return Task.FromResult(0);
        }

        static void sendMail(IdentityMessage message)
        {
            #region formatter
            string text = message.Body;
            string html = message.Body;
            #endregion

            MailMessage msg = new MailMessage();
            msg.To.Add(new MailAddress(message.Destination));
            msg.Subject = message.Subject;
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));
            msg.IsBodyHtml = true;
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Send(msg);
        }
    }
}