﻿
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Economiza.Servicios.Helper.TelemetryManager
{
    public class DeviceIoTTelemetryMsgService
    {
        private static EventHubClient eventHubClient;
        private const string EhConnectionString = "Endpoint=sb://devices-telemetry.servicebus.windows.net/;SharedAccessKeyName=in-telemetry-key;SharedAccessKey=r4N8SgynYgj3VuEjHO+qDDwKhOTnG2ggSr4jmaK+O8M=";
        private const string EhEntityPath = "inTelemetry";

        public void run(Dictionary<string, string> deviceMsg)
        {
            //var connectionStringBuilder = new EventHubsConnectionStringBuilder(EhConnectionString)
            //{
            //    EntityPath = EhEntityPath
            //};

            //eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());
            var eventHubClient = EventHubClient.CreateFromConnectionString(EhConnectionString, EhEntityPath);
            SendMessageToEventHub(deviceMsg);
            //eventHubClient.CloseAsync().Wait();
        }

        private static void SendMessageToEventHub(Dictionary<string, string> deviceMsg)
        {
            try
            {
                String deviceId = deviceMsg["deviceId"];
                float temperature = float.Parse(deviceMsg["oxygen"], CultureInfo.InvariantCulture.NumberFormat);
                float oxygen = float.Parse(deviceMsg["temperature"], CultureInfo.InvariantCulture.NumberFormat);
                var message = string.Format("{{\"deviceId\":\"{0}\",\"temperature\":{1},\"oxygen\":{2}}}", deviceId, temperature, oxygen);
                Console.WriteLine($"Sending message: {message}");
                eventHubClient.Send(new EventData(Encoding.UTF8.GetBytes(message)));
                Console.WriteLine("Message sent.");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} > Exception: {exception.Message}");
            }

        }
    }
}