﻿using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Economiza.Servicios.Helper.TelemetryManager
{
    public class DeviceIoTTelemetryService
    {
        public static async Task Run(Dictionary<string, string> deviceMsg)
        {
            Dictionary<string, string> devicesConnection = new Dictionary<string, string>();
            devicesConnection.Add("air-device-001", "HostName=iot-hub-devices.azure-devices.net;DeviceId=air-device-001;SharedAccessKey=IQF+XZGivhFz/iCVRBF4Gqjh6gh6OkYt4tmekDFHmFs=");
            devicesConnection.Add("air-device-002", "HostName=iot-hub-devices.azure-devices.net;DeviceId=air-device-002;SharedAccessKey=0wPprO6MdNgpQF1saE8ialQpqX7+th7lF52ld3mswWk=");
            devicesConnection.Add("air-device-003", "HostName=iot-hub-devices.azure-devices.net;DeviceId=air-device-003;SharedAccessKey=LuZYfgSo+2aHz4blGvc0CnGfYgIcqR2D6k2EZpPWqxM=");

            try
            {
                string DeviceConnectionString = devicesConnection[deviceMsg["deviceId"]];
                DeviceClient deviceClient = DeviceClient.CreateFromConnectionString(DeviceConnectionString, TransportType.Mqtt);

                await deviceClient.OpenAsync();
                SendEvent(deviceClient, deviceMsg);
                //ReceiveCommands(deviceClient).Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine("Error in sample: {0}", ex.Message);
            }
        }

        private static async void SendEvent(DeviceClient deviceClient, Dictionary<string, string> deviceMsg)
        {
            string dataBuffer;
            String deviceId = deviceMsg["deviceId"];
            double temperature = double.Parse(deviceMsg["temperature"]);
            double oxygen = double.Parse(deviceMsg["oxygen"]);

            TelemetryData data = new TelemetryData();
            data.deviceId = deviceId;
            data.oxygen = oxygen;
            data.temperature = temperature;
            
            dataBuffer = JsonConvert.SerializeObject(data);

            Message eventMessage = new Message(Encoding.UTF8.GetBytes(dataBuffer));
            Console.WriteLine("\t{0}> Sending message: Data: [{1}]", DateTime.Now.ToLocalTime(), dataBuffer);

            await deviceClient.SendEventAsync(eventMessage);
        }

        private class TelemetryData
        {
            public string deviceId;
            public double oxygen;
            public double temperature;
        }
    }
}