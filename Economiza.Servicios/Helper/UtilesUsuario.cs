﻿using System;
using System.Linq;
using System.Security.Claims;
using static Economiza.Servicios.Controllers.AccountController;
using Economiza.Business;
using Economiza.Entities;

namespace Economiza.Servicios.Helper
{
    public class UtilesUsuario
    {
        public UsuarioTO ObtenerUsuario()
        {
            var usuario = new UsuarioTO();
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(System.Web.HttpContext.Current.User.Identity as ClaimsIdentity);
            if (externalLogin != null)
            {
                usuario = AdministracionSsoMngr.GetInstance().obtenerUsuarioBySocial(externalLogin.LoginProvider, externalLogin.ProviderKey);
            }
            else
            {
                usuario = AdministracionSsoMngr.GetInstance().obtenerUsuarioByLocal(System.Web.HttpContext.Current.User.Identity.Name);
            }
            return usuario;
        }
    }
}