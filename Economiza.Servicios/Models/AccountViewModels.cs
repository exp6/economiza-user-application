﻿using System;
using System.Collections.Generic;

namespace Economiza.Servicios.Models
{
    // Modelos devueltos por las acciones de AccountController.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserInfoFullViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool HasRegistered { get; set; }
        public string LoginProvider { get; set; }

        public string DisplayName { get; set; }
        public string Picture { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public long nroKit { get; set; }
        public int idEstufa { get; set; }
        public int idModeloEstufa { get; set; }
        public int nroCohabitantes { get; set; }
        public int cantPromM3 { get; set; }
        public int cantPromMeses { get; set; }
        public bool withSocial { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
