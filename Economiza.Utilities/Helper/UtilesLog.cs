﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Economiza.Utilities
{
    public static class UtilesLog
    {
        public static void LogException(Exception e, string className)
        {
            Logger logger = LogManager.GetLogger(className);
            logger.Error(string.Format("Excepcion capturada: {0} Origen: {1} Mensaje: {2} StackTrace: {3}", e.GetType(), e.Source, e.Message, e.StackTrace));
        }

        public static void LogInfo(string mensaje, string className)
        {
            Logger logger = LogManager.GetLogger(className);
            logger.Info(mensaje);
        }

        public static void LogFatal(string mensaje, string className)
        {
            Logger logger = LogManager.GetLogger(className);
            logger.Fatal(mensaje);
        }
    }
}
